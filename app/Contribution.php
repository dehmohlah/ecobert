<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'member_id', 'payment_gateway', 'amount', 'transaction_id', 'transaction_ref',
    ];

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    /**
     * Define user table relationship
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
