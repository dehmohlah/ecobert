<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'loan_id', 'status', 'email', 'full_name', 'phone_number',
    ];

    /**
     * A role has one user
     *
     * @return object
     */
    public function loan()
    {
    	return $this->belongsTo('App\Loan');
    }

}
