<?php

namespace App\Http\Controllers\Auth;

use DB;
use Alert;
use App\User;
use Carbon\Carbon;
use \Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use PHPMailer\PHPMailer\PHPMailer;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller
{
   /*
   |--------------------------------------------------------------------------
   | Password Reset Controller
   |--------------------------------------------------------------------------
   |
   | This controller is responsible for handling password reset requests
   | and uses a simple trait to include this behavior. You're free to
   | explore this trait and override any methods you wish to tweak.
   |
   */

   use ResetsPasswords;

   /**
    * Create a new password controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('guest');
   }

   /**
    * Display the form to request a password reset link.
    *
    * @return \Illuminate\Http\Response
    */
   public function getEmail()
   {
       return view('auth.passwords.get-email');
   }

   /**
    * Send a reset link to the given user.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function postEmail(Request $request)
   {
      	$this->validate($request, ['email' => 'required|email']);

       // $response = Password::sendResetLink($request->only('email'), function (Message $message) {
       //     $message->subject($this->getEmailSubject());

       // });
      	if (User::where('email', '=', $request->email)->exists()) {
      		DB::table('password_resets')->insert([
		        'email' => $request->email,
		        'token' => $this->generateToken(),
		        'created_at' => Carbon::now()
		    ]);

		    $tokenData = DB::table('password_resets')
		    ->where('email', $request->email)->orderBy('created_at', 'desc')->first();

		   $token = $tokenData->token;
		   $email = $request->email;
		   $this->sendMail($token, $email);

		   return redirect()->back();
		} else {
			$response = "passwords.user";
			switch ($response) {
            case Password::RESET_LINK_SENT:
               return redirect()->back()->with('status', trans($response));

           case Password::INVALID_USER:
               return redirect()->back()->withErrors(['email' => trans($response)]);
       }
		}
   }

   /**
    * Display the password reset view for the given token.
    *
    * @param  string  $token
    * @return \Illuminate\Http\Response
    */
   public function getReset($token = null)
   {
       if (is_null($token)) {
           throw new NotFoundHttpException;
       }

       return view('auth.passwords.reset')->with('token', $token);
   }

   /**
    * Reset the given user's password.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function postReset(Request $request)
   {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        try {
		   	$appSecret    = getenv('APP_SECRET');
		    $jwt          = $request->token;   
		    $decodedToken = JWT::decode($jwt, $appSecret, ['HS256']);
        } catch (Exception $e) {
       	Alert::error("Token expired", "error");

       	return redirect()->back();
       }

       $password = $request->password;
	    $token = $request->token;

	    if (DB::table('password_resets')->where('email', $request->email)->exists()) {
		    $tokenData = DB::table('password_resets')
		     ->where('token', $token)->first();

		    $user = User::where('email', $tokenData->email)->first();

		    if ($user) {
		    	$this->resetPassword($user, $password);
		    	DB::table('password_resets')->where('email', $user->email)->delete();
		    	
		    	return redirect()->route('index');
		    }
		    Alert::error("Something went wrong, please try again", "error");

       		return redirect()->back();
	    } else {
	    	Alert::error("We could not not find the email in our records; Please use the same email we sent an email to. Thanks", "error", "error")->autoclose(5000);
	    	return redirect()->back();
		}
   }

   /**
    * Reset the given user's password.
    *
    * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
    * @param  string  $password
    * @return void
    */
   protected function resetPassword($user, $password)
   {
       $user->password = bcrypt($password);

       $user->save();

       Auth::login($user);
   }

   /**
     * Send mail to user
     *
     * @param  Request  $request
     * @return null
     */
    private function sendMail($token, $email)
    {
        $mail = new PHPMailer(true);

        $mail->isSMTP();
        $mail->Host = env('Host');
        $mail->SMTPAuth = true;
        $mail->Username = "testemail8534@gmail.com";//env('Username');
        $mail->Password = "londoner";//env('Password');
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->addAddress($email);
        $mail->From = 'ecoberty@gmail.com';
        $mail->FromName = "Ecoberty";
        
        $mail->isHTML(true);
        $mail->Subject = 'Ecoberty';
        $mail->Body    = view('emails.resetpassword', compact('token'));

        if(!$mail->send()) {
            Alert::error("Something went wrong, Please try again", "error")->persistent('Close');
            // return redirect()->back();
        } else {
            Alert::success("A password reset link have been sent to your email. Please go to your email and click on the link. It expires in the next 24 hours.", 'success')->autoclose(5000);

            // return redirect()->back();
        }
    }

    /**
     * Generate a token for user.
     *
     * @return string
     */
    public function generateToken()
    {
        $appSecret    = getenv('APP_SECRET');
        $jwtAlgorithm = getenv('JWT_ALGORITHM');
        $timeIssued   = time();
        $serverName   = getenv('SERVERNAME');
        $tokenId      = base64_encode(getenv('TOKENID'));
        $token        = [
            'iss'  => $serverName,    //Issuer: the server name
            'iat'  => $timeIssued,    // Issued at: time when the token was generated
            'jti'  => $tokenId,      // Json Token Id: an unique identifier for the token
            'nbf'  => $timeIssued,   //Not before time
            'exp'  => $timeIssued + 60 * 60 * 24, // expires in 24 hours
        ];

        return JWT::encode($token, $appSecret, $jwtAlgorithm);
    }
}