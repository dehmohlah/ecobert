<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use App\User;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Unicodeveloper\JusibePack\Facades\Jusibe as Jusibe;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
	/**
     * This method displays the signup page.
     *
     * @return signup page
     */
    public function registerForm()
    {
    	return view('auth.register');
    }

    /**
     * This method displays the login page.
     *
     * @return login page
     */
    public function loginForm()
    {
    	return view('auth.login');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Request  $request
     * @return User
     */
    public function create(RegisterRequest $request)
    {
    	User::create([
            'name'     => $request['name'],
            'email'    => $request['email'],
            'password' => bcrypt($request['password']),
            'gender'   => $request['gender'],
            'phone'    => $request['phone'],
            'address'  => $request['address'],
        ]);

        $this->sendSms([
        	'to' => $request->phone,
		    'from' => 'Ecoberty',
		    'message' => 'Thank you for indicating interest to join Ecoberty. Kindly note that membership form is 2500 naira. Please check your email in order to either download or fill online the membership online form. Please kindly check your email for other important message or call 07065116701 for more info. Thanks',
        ]);

        $this->sendMail($request);

        alert()->success('Your account has been successully created. Check your email for details about becoming a member', 'Success')->autoclose(3500);;

        Auth::attempt($request->only(['email', 'password']));

        return redirect()->route('index');
    }

    /**
     * Send sms
     *
     * @return back
     */
	public function sendSms($payload)
	{
        try {
            $response = Jusibe::sendSMS($payload)->getResponse();

            if ($response) {

                return redirect()->back();
            }

            return redirect()->back();
        } catch (Exception $e) {
            alert()->error('We are unable to send messages right now; your account has been created', 'Success');
        }
	}

	/**
     * Send mail to patient
     *
     * @param  Request  $request
     * @return dashboard
     */
    public function sendMail($data)
    {
        try {
            $mail = new PHPMailer(true);
            $name = $data->name;
            $email = $data->email;
            // dd($email);
            $mail->isSMTP();
            $mail->Host = env('Host');
            $mail->SMTPAuth = true;
            $mail->Username = "testemail8534@gmail.com";//env('Username');
            $mail->Password = "londoner";//env('Password');
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->addAddress($email);
            $mail->From = 'sexy_ng@yahoo.com';
            $mail->FromName = "Ecoberty";
            $mail->addStringAttachment(file_get_contents('http://res.cloudinary.com/ecoberty/image/upload/v1516724880/Application_Form-Ecoberty_kebxjd.pdf'), 'Membership-form.pdf');
            $mail->addStringAttachment(file_get_contents('http://res.cloudinary.com/ecoberty/image/upload/v1516736354/Ecoberty_Prospectus-1_1_zlwc8d.pdf'), 'Prospectus.pdf');

            $mail->isHTML(true);
            $mail->Subject = 'Ecoberty';
            $mail->Body    = view('emails.welcoming-email', compact('name', 'email'));

            if(!$mail->send()) {
                // alert()->error('Something went wrong'.$mail->ErrorInfo);
                return redirect()->back();
            } else {
                // alert()->success('Your email has been succesfully sent', 'Success');

                return redirect()->back();
            }

        } catch (Exception $e) {
            alert()->success('We are unable to send email right now; your account has been created', 'Success');
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Request  $request
     * @return User
     */
    public function login(LoginRequest $request)
    {
    	$authStatus = Auth::attempt($request->only(['email', 'password']), $request->has('remember'));

        if (!$authStatus) {
            Alert::error('Invalid email or Password, If you forget your password, click Forgot your Password?', 'Error')->autoclose(5000);

            return redirect()->back();
        }

        alert()->success('You are now signed in', 'Success');

        return redirect()->intended('/');
    }

    /**
     * logs user out.
     *
     * @return home
     */
    public function logOut()
    {
        Auth::logout();
        
        Alert::success('You have successully log out from your account', 'Good bye!');

        return redirect()->route('index');
    }
}
