<?php

namespace App\Http\Controllers;

use Auth;
use Paystack;
use App\User;
use App\Loan;
use Exception;
use App\Member;
use App\Guarantor;
use App\Contribution;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use App\Http\Requests\LoanApplyRequest;
use Unicodeveloper\JusibePack\Facades\Jusibe as Jusibe;

class DashboardController extends Controller
{
    public function index()
    {
        $totalMember = Member::all()->count();
        $totalUser = User::all()->count();
        $totalUnapprovedMember = Member::where('status', 1)->count();
        $totalApprovedMember = Member::where('status', 2)->count();
        $totalPendingMember = Member::where('status', 0)->count();
        $userContribution = Contribution::where('user_id', Auth::user()->id)->get();
        $ecobertyContribution = Contribution::all();
        if (isset(Auth::user()->member)) {
            $totalReferer = Member::where('introducer_number', Auth::user()->member->phone_number_1)->count();
        }

        $totalEcobertyContribution = 0;
        $totalContribution = 0;

        foreach ($userContribution as $key => $value) {
            $totalContribution += $value->amount;
        }

        foreach ($ecobertyContribution as $key => $value) {
            $totalEcobertyContribution += $value->amount;
        }

        return view('dashboard.index', compact('totalMember', 'totalUnapprovedMember', 'totalApprovedMember', 'totalPendingMember', 'totalUser', 'totalContribution', 'totalEcobertyContribution', 'totalReferer'));
    }


    public function loan()
    {
        $userContribution = Contribution::where('user_id', Auth::user()->id)->get();
        $totalEcobertyContribution = 0;
        $savings = 0;

        foreach ($userContribution as $key => $value) {
            $savings += $value->amount;
        }

        return view('dashboard.loan.savings', compact('savings'));
    }

    public function loanForm()
    {
        $member = Member::where('id', Auth::user()->member->id)->first();
        $userContribution = Contribution::where('user_id', Auth::user()->id)->get();
        $totalEcobertyContribution = 0;
        $savings = 0;
        $members = Member::all();

        foreach ($userContribution as $key => $value) {
            $savings += $value->amount;
        }

        return view('dashboard.loan.form', compact('savings', 'member', 'members'));
    }

    public function loanApply(LoanApplyRequest $request)
    {
        $loan = Loan::create([
            'member_id' => $request->member_id,
            'user_id' => Auth::user()->id,
            'amount'    => $request->amount,
            'name' => $request->name,
            'email' => $request->email,
            'bvn'   => $request->bvn,
            'account_name'  => $request->account_name,
            'account_number' => $request->account_number,
            'bank' => $request->bank,
            'purpose' => $request->purpose,
        ]);

        if ($loan) {
            foreach ($request->guarantor as $value) {
                $member = Member::find($value);
                $guarantor = Guarantor::create([
                    'loan_id' => $loan->id,
                    'email' => $member->email,
                    'full_name' => $member->first_name . " " . $member->last_name,
                    'phone_number' => $member->phone_number_1,
                ]);

                $this->sendSms([
                    'to' => $member->phone_number_1,
                    'from' => 'Ecoberty',
                    'message' => 'A member named ' . $request->name . ' has made you his/her guarantor, please check your email to either accept or reject the proposal. Thanks',
                ]);

                $this->sendMail($member, $request->name, $loan->id);
            }
            alert()->success('You have sucessfully apply for loan, the excos will review and get back to you', 'success')->persistent('Close');

            return redirect()->route('dashboard');
        }

        alert()->error('Something went wrong, please try again', 'Error');

        return redirect()->route('dashboard');
    }

    public function guarantorStatus(Request $request)
    {
        $guarantor = Guarantor::where('loan_id', $request->loanId)
            ->where('email', Auth::user()->member->email)
            ->first();

        if ($request->id == 1) {
            $guarantor->status = $request->id;
            $guarantor->save();
            alert()->success('You have sucessfully accepted this proposal', 'success');

            return redirect()->route('dashboard');
        }

        $guarantor->status = $request->id;
        $guarantor->save();
        alert()->success('You have sucessfully rejected this proposal', 'success');

        return redirect()->route('dashboard');
    }

    public function paymentForm(Request $request)
    {
    	if ($request->purpose == "reg") {
    		$purpose = "Registration Fee";
    	} else {
    		$purpose = "Contribution Fee";
    	}

    	return view('dashboard.payment.paystack-payment-form', compact('purpose'));
    }

    /**
     * Send sms
     *
     * @return back
     */
    public function sendSms($payload)
    {
        try {
            $response = Jusibe::sendSMS($payload)->getResponse();

            if ($response) {

                return redirect()->back();
            }

            return redirect()->back();
        } catch (Exception $e) {
            alert()->error('We are unable to send text messages right now', 'Error');
        }
    }

    /**
     * Send mail to patient
     *
     * @param  Request  $request
     * @return dashboard
     */
    public function sendMail($data, $loanerName, $loanId)
    {
        try {
            $mail = new PHPMailer(true);
            $name = $data->last_name . " " . $data->first_name;
            $email = $data->email;
            $request = "proposal";
            $mail->isSMTP();
            $mail->Host = env('Host');
            $mail->SMTPAuth = true;
            $mail->Username = "testemail8534@gmail.com";//env('Username');
            $mail->Password = "londoner";//env('Password');
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->addAddress($email);
            $mail->From = 'ecoberty@gmail.com';
            $mail->FromName = "Ecoberty";

            $mail->isHTML(true);
            $mail->Subject = 'Ecoberty';
            $mail->Body    = view('emails.members-notification', compact('name', 'email', 'request', 'loanerName', 'loanId'));

            if(!$mail->send()) {
                // alert()->error('Something went wrong'.$mail->ErrorInfo);
                return redirect()->back();
            } else {
                // alert()->success('Your email has been succesfully sent', 'Success');
                return redirect()->back();
            }

        } catch (Exception $e) {
            alert()->error('We are unable to send email right now', 'error');
        }
    }
}
