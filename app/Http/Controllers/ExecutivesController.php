<?php

namespace App\Http\Controllers;

// use PDF;
use Alert;
use App\User;
use Cloudder;
use Exception;
use App\Loan;
use App\Member;
use Carbon\Carbon;
use App\Guarantor;
use App\Contribution;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateMemberRequest;
use Unicodeveloper\JusibePack\Facades\Jusibe as Jusibe;

class ExecutivesController extends Controller
{
    public function users()
    {
    	$users = User::orderBy('created_at', 'DESC')->paginate(10);

    	return view('dashboard.executives.list-users', compact('users'));
    }

    public function members()
    {
        $status = [
            0 => "Not Approved",
            1 => "Dissapproved",
            2 => "Approved",
        ];

    	$members = Member::orderBy('created_at', 'DESC')->paginate(10);
        $trashedMembers = Member::onlyTrashed()->paginate(10);

    	return view('dashboard.executives.list-members', compact('members', 'trashedMembers', 'status'));
    }

    public function unapprovedMembers()
    {
        $status = [
            0 => "Not Approved",
            1 => "Dissapproved",
            2 => "Approved",
        ];

        $members = Member::where('status', 1)->orderBy('created_at', 'DESC')->paginate(10);
        $trashedMembers = Member::onlyTrashed()->paginate(10);

        return view('dashboard.executives.list-members', compact('members', 'trashedMembers', 'status'));   
    }

    public function excos()
    {
    	$excos = User::where('role_id', 3)->paginate(10);

    	return view('dashboard.executives.excos', compact('excos'));
    }

    public function loanApplications()
    {
        $loans = Loan::orderBy('created_at', 'DESC')->paginate(10);


        return view('dashboard.executives.loans', compact('loans'));
    }

    public function loanApplicationDetail(Request $request)
    {
        $loan = Loan::find($request->id);
        $guarantors = Guarantor::where('loan_id', $loan->id)->get();
        $member = Member::where('id', $loan->member_id)->first();
        $contributions = Contribution::where('member_id', $loan->member_id)->get();
        $savings = 0;
        foreach ($contributions as $key => $value) {
            $savings += $value->amount;
        }

        if ($loan) {
            return view('dashboard.executives.loan-details', compact('savings', 'loan', 'guarantors', 'member', 'contributions')); 
        }

        Alert::error("Loan not found", "error");
        return redirect()->back();
    }

    public function viewMember(Request $request)
    {
        $member = Member::find($request->id);
        if ($member) {
            return view('dashboard.member.view-member-details', compact('member'));
        }

        Alert::error("Member not found", "error");
        return redirect()->back();
    }

    public function editMember(Request $request)
    {
    	$member = Member::find($request->id);
        if ($member) {
    	   return view('dashboard.member.edit-form', compact('member'));
        }

        Alert::error("Member not found", "error");
        return redirect()->back();
    }

    public function editUser(Request $request)
    {
        $user = User::find($request->id);
        if ($user) {
            return view('dashboard.executives.edit-user-form', compact('user'));
        }

        Alert::error("User not found", "error");
        return redirect()->back();
    }

    public function editExco(Request $request)
    {
        $user = User::find($request->id);
        if ($user) {
            return view('dashboard.executives.edit-exco-form', compact('user'));
        }

        Alert::error("User not found", "error");
        return redirect()->back();
    }

    public function updateUser(UpdateUserRequest $request)
    {
        $updateUser = User::where('id', $request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'gender' => $request->gender,
            'address' => $request->address,
            'phone' => $request->phone,
            'avatar' => $request->avatar,
        ]);

        if ($updateUser) {
            Alert::success('Users succesfully updated', 'success');
            
            return redirect()->back();
        }

        Alert::error('Something went wrong, please try again', 'error');

        return redirect()->back();
    }

    // public function exportPdf(Request $request)
    // {
    //     $member = Member::find($request->id);
    //     $pdf = PDF::loadView('dashboard.member.view-member-details', compact('member'));

    //     return $pdf->stream();
    // }

    public function approveOrDissaproveMember(Request $request)
    {
    	$member= Member::find($request->id);
        if ($member) {
            if (isset($request->reason)) {
                $member->status = 1;
                $check = $member->save();

                if ($check) {
                    Alert::success("You have successfully Dissapproved " . $member->last_name . " " . $member->first_name, "Success")->persistent('Close');
                    
                    $this->sendSms([
                        'to' => $member->phone_number_1,
                        'from' => 'Ecoberty',
                        'message' => 'Oops, ' . $member->last_name . ' ' . $member->first_name . ' You have been DISSAPROVED by one of the excos. Please check your email to see reason and make ammendment. Thanks',
                    ]);

                    $this->sendMail($member, $request);

                    return redirect()->route('members');
                }

                Alert::error("Something went wrong, please try again ", "Error");
                return redirect()->route('members');
            } else if (isset($request->reset)) {
                $member->status = 0;
                $check = $member->save();

                if ($check) {
                    Alert::success("You have successfully pend " . $member->last_name . " " . $member->first_name, "Success")->persistent('Close');
                    
                    $this->sendSms([
                        'to' => $member->phone_number_1,
                        'from' => 'Ecoberty',
                        'message' => 'Oops, ' . $member->last_name . ' ' . $member->first_name . ' Your status has been changed to pending by one of the excos. Please check your email to see reason and make ammendment. Thanks',
                    ]);

                    $this->sendMail($member, $request);

                    return redirect()->route('members');
                }

                Alert::error("Something went wrong, please try again ", "Error");
                return redirect()->route('members');
            } else {
                $member->status = 2;
                $member->approval_date = Carbon::now()->toDateTimeString();
                $check = $member->save();

                if ($check) {
                    Alert::success("You have successfully Approved " . $member->last_name . " " . $member->first_name . "notification email has been sent to user", "Success")->persistent('Close');
                    $this->sendSms([
                        'to' => $member->phone_number_1,
                        'from' => 'Ecoberty',
                        'message' => 'Congratulation ' . $member->last_name . ' ' . $member->first_name . ' You have been approved by one of the excos. Please check your dashboard to make your first contribution. Thanks',
                    ]);

                    $this->sendMail($member, null);

                    return redirect()->route('members');
                }

                Alert::error("Something went wrong, please try again ", "Error");

                return redirect()->route('members');
            }
        }

        Alert::error("User not found", "error");
        return redirect()->back();
    }

    public function updateMember(UpdateMemberRequest $request)
    {
    	$updateMember = Member::where('id', $request->id)->update([
    		'title' => $request->title,
    		'last_name' => $request->last_name,
    		'first_name' => $request->first_name,
    		'email' => $request->email,
    		'status' => $request->status,
    		'gender' => $request->gender,
    		'contact_address' => $request->contact_address,
    		'postal_address' => $request->postal_address,
    		'phone_number_1' => $request->phone_number_1,
    		'phone_number_2' => $request->phone_number_2,
    		'passport' => isset($request->avatar) ? $this->postImage($request->avatar) : $request->passport,
    		'date_of_birth' => $request->date_of_birth,
    		'place_of_birth' => $request->place_of_birth,
    		'monthly_contribution' => $request->monthly_contribution,
    	]);

    	if ($updateMember) {
    		Alert::success('Members succesfully updated', 'success');
    		
    		return redirect()->back();
    	}

    	Alert::error('Something went wrong, please try again', 'error');

    	return redirect()->back();
    }

    /**
     *  Posts image update request.
     */
    private function postImage($img)
    {
        Cloudder::upload($img, null);

        return Cloudder::getResult()['url'];
    }

    public function deleteMember(Request $request)
    {
    	$deleteMember = Member::where('id', $request->id)->delete();

    	if ($deleteMember) {
    		Alert::success('The member has been successfully deleted', 'success');

    		return redirect()->back();
    	}

    	Alert::error('Something went wrong, please try again', 'error');

    	return redirect()->back();
    }

    public function restoreMember(Request $request)
    {
        $restoreMember = Member::where('id', $request->id)->restore();

        if ($restoreMember) {
            Alert::success('The member has been successfully restored', 'success');

            return redirect()->back();
        }

        Alert::error('Something went wrong, please try again', 'error');

        return redirect()->back();
    }

    public function acceptLoan(Request $request)
    {
        $loan = Loan::find($request->id);
        $member = Member::find($loan->member_id);
        
        if ($loan) {
            if ($loan->status == "accepted") {
                Alert::error("Loan already accepted", "error");

                return redirect()->back();
            }

            $loan->status = "accepted";
            $loan->save();
            $this->sendSms([
                'to' => $member->phone_number_1,
                'from' => 'Ecoberty',
                'message' => 'Congratulation ' . $member->last_name . ' ' . $member->first_name . ' Your loan have been accepted by one of the excos. Thanks',
            ]);

            $this->sendMail($member, "loan-accept");

            return redirect()->route('loan-applications');
        }

        Alert::error("Loan not found", "error");
        return redirect()->back();
        
    }

    public function rejectLoan(Request $request)
    {
        $loan = Loan::find($request->id);
        $member = Member::find($loan->member_id);
        // $reason = $request->reason;
        
        if ($loan) {
            if ($loan->status == "rejected") {
                Alert::error("Loan already rejected", "error");

                return redirect()->back();
            }

            $loan->status = "rejected";
            $loan->save();
            $this->sendSms([
                'to' => $member->phone_number_1,
                'from' => 'Ecoberty',
                'message' => 'Oops ' . $member->last_name . ' ' . $member->first_name . ' Your loan was rejected by one of the excos. Please check your email to see reasons. Call 07065116701 for more info. Thanks.',
            ]);

            $this->sendMail($member, $request);

            return redirect()->route('loan-applications');
        }

        Alert::error("Loan not found", "error");
        return redirect()->back();
        
    }

    /**
     * Send sms
     *
     * @return back
     */
	public function sendSms($payload)
	{
        try {
            $response = Jusibe::sendSMS($payload)->getResponse();

            if ($response) {

                return redirect()->back();
            }

            return redirect()->back();
        } catch (Exception $e) {
            alert()->error('We are unable to send text messages right now', 'Error');
        }
	}

	/**
     * Send mail
     *
     * @param  Request  $request
     * @return dashboard
     */
    public function sendMail($data, $request)
    {
        try {
            $mail = new PHPMailer(true);
            $name = $data->last_name . " " . $data->first_name;
            $email = $data->email;
            $mail->isSMTP();
            $mail->Host = env('Host');
            $mail->SMTPAuth = true;
            $mail->Username = "testemail8534@gmail.com";//env('Username');
            $mail->Password = "londoner";//env('Password');
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->addAddress($email);
            $mail->From = 'ecoberty@gmail.com';
            $mail->FromName = "Ecoberty";

            $mail->isHTML(true);
            $mail->Subject = 'Ecoberty';
            $mail->Body    = view('emails.members-notification', compact('name', 'email', 'request'));

            if(!$mail->send()) {
                // alert()->error('Something went wrong'.$mail->ErrorInfo);
                return redirect()->back();
            } else {
                // alert()->success('Your email has been succesfully sent', 'Success');
                return redirect()->back();
            }

        } catch (Exception $e) {
            alert()->error('We are unable to send email right now', 'error');
        }
    }
}
