<?php

namespace App\Http\Controllers;


use Auth;
use Alert;
use App\User;
use Cloudder;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Http\Requests\MemberPage1Request;
use App\Http\Requests\MemberPage2Request;

class MembersController extends Controller
{
    public function membersForm()
    {
        return view('members.form');
    }

    public function membersFormNext()
    {
        return view('members.form2');
    }

    private function postFirstpageMembershipForm($data)
    {
        $user = User::where('email', $data['email'])->first();

        if (is_null($user)) {
            $password = $data['last_name'] . rand();
            $user = User::create([
                'name' => $data['last_name'] . " " . $data['first_name'],
                'password' => bcrypt($password),
                'email'    => $data['email'],
                'gender'   => $data['gender'],
                'address'  => $data['address'],
                'phone'    => $data['phone_number_1'],
                'role_id' => 2,
            ]);

            $this->sendMail($data, $password);
            Auth::attempt(['email' => $data['email'], 'password' => $password]);
        }

        if ($user->role_id === 1) {
            $user->increment('role_id');
        }

        return Member::create([
            'user_id'  => $user->id,
            'title'    => $data['title'],
            'first_name'    => $data['first_name'],
            'last_name'    => $data['last_name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
            'gender'   => $data['gender'],
            'passport' => $this->postImage($data->file('avatar')),
            'contact_address'  => $data['contact_address'],
            'phone_number_1'    => $data['phone_number_1'],
            'phone_number_2'    => $data['phone_number_2'],
            'postal_address'  => $data['postal_address'],
            'date_of_birth'  => $data['date_of_birth'],
            'place_of_birth'    => $data['place_of_birth'],
            'nationality'    => $data['nationality'],
            'home_town_village_of_origin'    => $data['home_town_village_of_origin'],
            'local_government_area'    => $data['local_government_area'],
            'name_of_next_of_kin' => $data['name_of_next_kin'],
            'address_of_next_of_kin'   => $data['address_of_next_kin'],
            'phone_of_next_of_kin' => $data['phone_of_next_kin'],

            'company_address'  => "null",
            'date_join_company'    => Carbon::now(),
            'post'    => "null",
            'responsibilities'    => "null",
            'monthly_contribution'    => "null",
            'introduced_by'    => "null",
            'introducer_number'    => "null",
            'signature' => "null",
        ]);
    }

    public function postMembershipForm(MemberPage1Request $request)
    {
        if (array_key_exists("saveContinueLater", $request->all())) {
            $member = $this->postFirstpageMembershipForm($request);
            if ($member) {
                Alert::success('You have successully save your membership', 'Success');

                return redirect()->route('index');
            }
            Alert::error('Something went wrong, please try again or call 07065116701', 'Error');

            return redirect()->back();
        } else {
            $member = $this->postFirstpageMembershipForm($request);
            if ($member) {
                Alert::success('You have successully save your membership', 'Success');

                return redirect()->route('members-form-next');
            }
            Alert::error('Something went wrong, please try again or call 07065116701', 'Error');

            return redirect()->back();
        }
    }

    public function postMembersFormNext(MemberPage2Request $request)
    {
        $imagedata = base64_decode($request['img_data']);
        $name = md5(date("dmYhisA"));
        
        $fileName = $name .'.png';
        file_put_contents($fileName,$imagedata);

        Member::where('email', $request['email'])->update([
            'company_address'  => $request['company_address'],
            'date_join_company'    => $request['date_join_company'],
            'post'    => $request['post'],
            'completed' => 1,
            'responsibilities'    => $request['responsibilities'],
            'monthly_contribution'    => $request['monthly_contribution'],
            'introduced_by'    => $request['introduced_by'],
            'introducer_number'    => $request['introducer_number'],
            'signature' => $this->postImage($fileName),
        ]);

        unlink($fileName);

        return response()->json(['message' => 'succesfully updated'], 201);
    }

    /**
     *  Posts image update request.
     */
    private function postImage($img)
    {
        
        Cloudder::upload($img, null);
        

        return Cloudder::getResult()['url'];
    }

    /**
     * Send mail to user
     *
     * @param  Request  $request
     * @return null
     */
    private function sendMail($data, $password)
    {
        $mail = new PHPMailer(true);
        $name = $data['last_name'] . " " . $data['first_name'];
        $email = $data->email;

        $mail->isSMTP();
        $mail->Host = env('Host');
        $mail->SMTPAuth = true;
        $mail->Username = "testemail8534@gmail.com";//env('Username');
        $mail->Password = "londoner";//env('Password');
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->addAddress($email);
        $mail->From = 'ecoberty@gmail.com';
        $mail->FromName = "Ecoberty";
        
        $mail->isHTML(true);
        $mail->Subject = 'Ecoberty';
        $mail->Body    = view('emails.user-details-email', compact('name', 'email', 'password'));

        if(!$mail->send()) {
            Alert::error("Email isn't going right now... Your login details are Email: " . $email . "Password: " . $password, "error")->persistent('Close');
            // return redirect()->back();
        } else {
            alert()->success('Check your email for your login details', 'Success');

            // return redirect()->back();
        }
    }
}
