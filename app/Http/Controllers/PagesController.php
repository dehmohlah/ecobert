<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Mail\Mailer as Mail;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function termsAndConditions()
    {
        return view('pages.terms-condition');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function services()
    {
        return view('pages.services');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function mailForm(Request $request)
    {

        try {
            $this->mail->send();
            
            return 'Email sent succesfully to user';
        } catch (\Exception $e) {
            return 'Failure ' . $e->getMessage();
        }
    }

    public function membershipForm()
    {
        return view('pages.membershipform');
    }

}
