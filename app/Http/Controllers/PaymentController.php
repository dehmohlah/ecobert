<?php

namespace App\Http\Controllers;

use Alert;
use Paystack;
use App\User;
use Exception;
use App\Member;
use App\Contribution;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ManualRecordRequest;


class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        try {
            $paymentDetails = Paystack::getPaymentData();

            if ($paymentDetails['data']['status'] == "success") {
                if ($paymentDetails['data']['metadata']['purpose'] == "registration-fee") {
                    return $this->saveRegData($paymentDetails['data']);
                } else {
                    return $this->saveContributionData($paymentDetails['data']);
                }
            } else {
                Alert::error('Something went wrong; Payment was declined');
                return redirect()->route('dashboard');
            }
        } catch (Exception $e) {
            Alert::error('Something went wrong; Payment was declined');

            return redirect()->route('dashboard');
        }
    }

    private function saveRegData($data)
    {
    	$member = Member::where('id', $data['metadata']['member_id'])->update([
            'reg_payment' => 1,
            'reg_payment_transaction_id' => $data['id'],
            'reg_payment_transaction_ref' => $data['reference'],
            'reg_payment_date' => $data['transaction_date'],
        ]);

        if ($member) {
            Alert::success("Your payment was succesfull", "Success");
            return redirect()->route('dashboard');
        } else {
            Alert::error("Something went wrong", "Error");
            return redirect()->route('dashboard'); 
        }
    }

    private function saveContributionData($data)
    {
    	$contribution = Contribution::create([
            'user_id' => $data['metadata']['user_id'],
            'member_id' => $data['metadata']['member_id'],
            'payment_gateway' => isset($data['mode_of_payment']) ? $data['mode_of_payment'] : 'Paystack',
            'amount' => $data['amount']/100,
            'transaction_id' => $data['id'],
            'transaction_ref' => $data['reference'],
        ]);

        if ($contribution) {
            Alert::success("Your payment was succesfull", "Success");
            return redirect()->route('dashboard');
        } else {
            Alert::error("Something went wrong", "Error");
            return redirect()->route('dashboard'); 
        }
    }

    public function manualPayment(Request $request)
    {
        if ($request->purpose == "reg") {
            $purpose = "Registration Fee";
        } else {
            $purpose = "Contribution Fee";
        }

        return view('dashboard.payment.manual-payment-form', compact('purpose'));
    }

    public function postManualPayment(ManualRecordRequest $request)
    {
        if (Member::where('email', $request->email)->exists()) {
            $paymentDetails = $request->all();
            if ($request->purpose == "registration-fee") {
                $member = Member::where('email', $request->email)->first();
                $paymentDetails['metadata']['member_id'] = $member->id;

                return $this->saveRegData($paymentDetails);
            } else {
                $member = Member::where('email', $request->email)->first();
                $paymentDetails['metadata']['member_id'] = $member->id;
                $paymentDetails['metadata']['user_id'] = $member->user_id;

                return $this->saveContributionData($paymentDetails);
            }
        }

        Alert::error("There is no user associated with the email", "error");
        return redirect()->back();
    }
}
