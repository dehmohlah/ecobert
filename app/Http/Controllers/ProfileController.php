<?php

namespace App\Http\Controllers;

use Hash;
use Auth;
use Alert;
use App\User;
use Cloudder;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\PostChangePasswordFormRequest;

class ProfileController extends Controller
{
    public function getProfileSettings(Request $request)
    {
    	$user = Auth::user()->first();

        return view('dashboard.profile.settings', compact('user'));
    }

    public function updateProfileSettings(UpdateProfileRequest $request)
    {
        $updateUser = User::where('id', $request->id)->update([
            'name' => $request->name,
            'gender' => $request->gender,
            'address' => $request->address,
            'phone' => $request->phone,
            'avatar' => $request->avatar,
        ]);

        if ($updateUser) {
            Alert::success('You have succesfully updated your settings', 'success');
            
            return redirect()->back();
        }

        Alert::error('Something went wrong, please try again', 'error');

        return redirect()->back();
    }

    /**
     *  Posts image update request.
     */
    private function postImage($img)
    {
        Cloudder::upload($img, null);

        return Cloudder::getResult()['url'];
    }

    /**
     *  get change password page
     */
    public function getChangePassword()
    {
        $users = Auth::user();

        return view('dashboard.profile.changepassword', compact('users'));
    }

    /**
     *  Post change password request.
     */
    public function postChangePassword(PostChangePasswordFormRequest $request)
    {
        $user = Auth::user();

        // Compare old password
        if (!Hash::check($request->oldPassword, $user->password)) {
            alert()->error('Old password incorrect', 'error');

            return redirect()->back();
        }

        // Update current password
        $user->password = Hash::make($request->password);
        $user->save();

        alert()->success('Password successfully updated', 'success');

        return redirect()->route('dashboard');
    }
}
