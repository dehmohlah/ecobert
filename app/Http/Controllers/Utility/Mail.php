<?php

namespace App\Http\Controllers\Utility;

use App\User;
use Exception;
use App\Member;
use PHPMailer\PHPMailer\PHPMailer;

trait Mail
{
	/**
     * Send mail to user
     *
     * @param  Request  $request
     * @return null
     */
    public function sendMail($data, $view, $error, $success)
    {
        $mail = new PHPMailer(true);
        $name = $data['last_name'] . " " . $data['first_name'];
        $email = $data->email;

        $mail->isSMTP();
        $mail->Host = env('Host');
        $mail->SMTPAuth = true;
        $mail->Username = "testemail8534@gmail.com";//env('Username');
        $mail->Password = "londoner";//env('Password');
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->addAddress($email);
        $mail->From = 'ecoberty@gmail.com';
        $mail->FromName = "Ecoberty";
        
        $mail->isHTML(true);
        $mail->Subject = 'Ecoberty';
        $mail->Body    = view($view, compact('name', 'email', 'password'));

        if(!$mail->send()) {
            Alert::error($error, 'error')->persistent('Close');
            // return redirect()->back();
        } else {
            alert()->success($success, 'Success');

            // return redirect()->back();
        }
    }
}