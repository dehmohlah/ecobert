<?php

namespace App\Http\Middleware;

use Alert;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class CheckMember
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param Guard $auth
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = $this->auth->user();

        if (is_null($user)) {
            return redirect()->guest('login');
        }

        if ($user->role_id === 1) {
            Alert::error('You have no access to visit the dasboard, only members can view the dashboard', 'Error');

            return redirect()->route('index');
        }

        return $next($request);
    }
}
