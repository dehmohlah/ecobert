<?php

namespace App\Http\Middleware;

use Alert;
use Closure;
use Carbon\Carbon;
use App\Contribution;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class LoanApply
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param Guard $auth
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $member = $this->auth->user()->member;
        $approvedDate = new Carbon($member->approval_date);

        $loanMonth = $approvedDate->addMonths(6);
        $today = Carbon::now();

        if ($loanMonth >= $today) {
            $contributions = Contribution::where('member_id', $member->id)->get();
            if ($contributions->count() >= 6) {
                return $next($request);
            }
        }

        Alert::error('You have no access to apply for loan yet, you have to make contribution consistently for 6 months to unlock your access ', 'Error')->persistent('Close');

        return redirect()->route('dashboard');
    }
}
