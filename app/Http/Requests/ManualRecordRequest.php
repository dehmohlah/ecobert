<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ManualRecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->purpose == "registration-fee") {
            return [
                'email' => 'required',
                'id'     => 'required',
                'reference' => 'required',
                'transaction_date'     => 'required',
                'mode_of_payment' => 'required',
            ];
        } else {
            return [
                'email' => 'required',
                'id'     => 'required',
                'reference' => 'required',
                'transaction_date'     => 'required',
                'mode_of_payment' => 'required',
                'amount' => 'required',
            ];
        }
    }
}
