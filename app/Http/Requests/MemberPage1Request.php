<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberPage1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar'    => 'required|image|max:10240',
            'title'    => 'required',
            'gender'    => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email'     => 'required|max:255|unique:members,email',
            'contact_address' => 'required',
            'phone_number_1'     => 'required|regex:/(0)[0-9]{10}/|unique:members,phone_number_1',
            'postal_address' => 'required',
            'date_of_birth' => 'required',
            'place_of_birth' => 'required',
            'nationality' => 'required',
            'state_of_origin' => 'required',
            'home_town_village_of_origin' => 'required',
            'local_government_area'  => 'required',
            'name_of_next_kin' => 'required',
            'address_of_next_kin'    => 'required',
            'phone_of_next_kin' => 'required|regex:/(0)[0-9]{10}/',
        ];
    }
}
