<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberPage2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_address'      => 'required',
            'post'     => 'required',
            'date_join_company'     => 'required',
            'responsibilities'  => 'required',
            'monthly_contribution'    => 'required',
            'introduced_by'   => 'required',
            'introducer_number'   => 'required|regex:/(0)[0-9]{10}/',
        ];
    }
}
