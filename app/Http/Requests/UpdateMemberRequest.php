<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ($request->avatar == null) {
            return [
                'title'    => 'required',
                'gender'    => 'required',
                'status'    => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email'     => 'required|max:255|unique:members,email,'.$request->id,
                'contact_address' => 'required',
                'phone_number_1'     => 'required|regex:/(0)[0-9]{10}/|unique:members,phone_number_1,'.$request->id,
                'postal_address' => 'required',
                'date_of_birth' => 'required',
                'place_of_birth' => 'required',
                'monthly_contribution' => 'required',
            ];
        } else {
            return [
                'avatar' => 'required|image|max:10240',
                'title'    => 'required',
                'gender'    => 'required',
                'status'    => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email'     => 'required|max:255|unique:members,email,'.$request->id,
                'contact_address' => 'required',
                'phone_number_1'     => 'required|regex:/(0)[0-9]{10}/|unique:members,phone_number_1,'.$request->id,
                'postal_address' => 'required',
                'date_of_birth' => 'required',
                'place_of_birth' => 'required',
                'monthly_contribution' => 'required',
            ];
        }
    }
}
