<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required',
            'email'     => 'required|max:255|unique:users,email,'.$request->id,
            'address' => 'required',
            'phone'     => 'required|regex:/(0)[0-9]{10}/|unique:users,phone,'.$request->id,
        ];
    }
}
