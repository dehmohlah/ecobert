<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'user_id', 'name', 'email', 'bvn', 'amount', 'account_number', 'account_name', 'bank', 'purpose', 'pending'   
    ];

    /**
     * A role has one user
     *
     * @return object
     */
    public function guarantors()
    {
    	return $this->hasMany('App\Guarantor');
    }

}
