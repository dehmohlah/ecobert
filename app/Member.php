<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
     use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'user_id',
        'surname',
        'first_name',
        'last_name',
        'email',
        'password',
        'gender',
        'passport',
        'contact_address',
        'phone_number_1',
        'phone_number_2',
        'completed',
        'postal_address',
        'date_of_birth',
        'place_of_birth',
        'nationality',
        'home_town_village_of_origin',
        'local_government_area',
        'name_of_next_of_kin',
        'address_of_next_of_kin',
        'phone_of_next_of_kin',
        'company_address',
        'date_join_company',
        'post',
        'responsibilities',
        'monthly_contribution',
        'introduced_by',
        'introducer_number',
        'signature',
        'reg_payment',
        'reg_payment_transaction_id',
        'reg_payment_transaction_ref',
        'reg_payment_date',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at']; 

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
     * Define contribution table relationship
     *
     * @return object
     */
    public function contribution()
    {
        return $this->hasMany('App\Contribution');
    }
}
