<?php

namespace App\Providers;

use App\User;
use App\Member;
use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('registered-user', function ($user) {
            return $user->role_id != 1;
        });

        Gate::define('members', function ($user) {
            return $user->role_id == 2;
        });

        Gate::define('become-members', function ($user) {
            return $user->role_id == 1;
        });

        Gate::define('incomplete-members', function ($user) {
            return isset($user->member) ? $user->member->completed == 0 : "";
        });

        Gate::define('complete-members', function ($user) {
            return isset($user->member) ? $user->member->completed != 0 : "";
        });

        Gate::define('not-paid-reg-fee', function ($user) {
            return isset($user->member) ? $user->member->reg_payment == 0 : "";
        });

        Gate::define('not-approved-member', function ($user) {
            return isset($user->member) ? $user->member->status == 0 : "";
        });

        Gate::define('paid-reg-fee', function ($user) {
            return isset($user->member) ? $user->member->reg_payment == 1 : "";
        });

        Gate::define('exco-user', function ($user) {
            return $user->role_id == 3;
        });
    }
}
