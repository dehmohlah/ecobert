<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',    
    ];

    /**
     * A role has one user
     *
     * @return object
     */
    public function user()
    {
    	return $this->hasMany('App\User');
    }

}
