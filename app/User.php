<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'phone', 'address', 'avatar', 'role_id', 'provider', 'provider_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Define roles table relationship
     *
     * @return object
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * Define members table relationship
     *
     * @return object
     */
    public function member()
    {
        return $this->hasOne('App\Member');
    }

    /**
     * Define Contribution table relationship
     *
     * @return object
     */
    public function contribution()
    {
        return $this->hasMany('App\Contribution');
    }
}
