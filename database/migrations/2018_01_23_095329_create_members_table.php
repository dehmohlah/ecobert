<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('gender');
            $table->string('passport');
            $table->string('contact_address');
            $table->string('phone_number_1')->unique();
            $table->string('phone_number_2')->nullable();
            $table->string('completed')->default(0);
            $table->string('status')->default(0);
            $table->string('postal_address');
            $table->string('date_of_birth');
            $table->string('place_of_birth');
            $table->string('nationality');
            $table->string('home_town_village_of_origin')->nullable();
            $table->string('local_government_area');
            $table->string('name_of_next_of_kin');
            $table->string('address_of_next_of_kin');
            $table->string('phone_of_next_of_kin');
            $table->string('company_address');
            $table->date('date_join_company');
            $table->string('post');
            $table->text('responsibilities');
            $table->string('monthly_contribution');
            $table->string('introduced_by');
            $table->string('introducer_number');
            $table->string('signature');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
