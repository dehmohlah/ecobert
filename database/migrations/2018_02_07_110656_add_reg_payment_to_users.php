<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegPaymentToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function($table) {
            $table->string('reg_payment')->after('signature')->default(0);
            $table->string('reg_payment_transaction_id')->after('reg_payment')->nullable();
            $table->string('reg_payment_transaction_ref')->after('reg_payment_transaction_id')->unique()->nullable();
            $table->string('reg_payment_date')->after('reg_payment_transaction_ref')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function($table) {
            $table->dropColumn('reg_payment');
            $table->dropColumn('reg_payment_transaction_id');
            $table->dropColumn('reg_payment_transaction_ref');
        });
    }
}
