<?php

use App\Role;
use Illuminate\Database\Seeder;

class GlobalSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'NORMAL',
        ]);
        Role::create([
            'name' => 'MEMBER',
        ]);
        Role::create([
            'name' => 'EXCO',
        ]);
    }
}
