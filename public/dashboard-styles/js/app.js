$('.approve-member').on("change", function (event) {
    var _this = $(this);
    var id  = $(this).data('id');
    var value = _this.val();
    if (value == "Approved") {
        if (confirm('Are you sure you want to approve this member?')) {
            
            window.location.href = "/approve-member/"+id;
        }
    } else if(value == "Dissapproved") {
        if (confirm('Are you sure you want to dissaprove this member?')) {
            var reason = prompt("Please enter your reason");
            if (reason == "") {
                alert("Please input a reason");
                window.location.href ="/members";
            } else {
                window.location.href = "/approve-member/"+id+"?reason="+reason;
            }
        }
    } else {
        if (confirm('Are you sure you want to change this member status to pending?')) {
            var reason = prompt("Please enter your reason");
            if (reason == "") {
                alert("Please input a reason");
                window.location.href ="/members";
            } else {
                window.location.href = "/approve-member/"+id+"?reset=reason";
            }
        }
    }

    event.preventDefault();
});

$('.delete-member').click(function (event) {
    var id  = $(this).data('id');

    if (confirm('Are you sure you want to Delete this member?')) {
        var id = $(this).data('id');
        console.log(id);

        window.location.href = '/delete-member/'+id;
    }

    event.preventDefault();
});

$('.restore-member').click(function (event) {
    var id  = $(this).data('id');

   if (confirm('Are you sure you want to restore this member?')) {
            var id = $(this).data('id');
            window.location.href = '/restore-member/'+id;
    }

    event.preventDefault();
});

$('#exportButton').click(function () {
    console.log("I am here");
    var doc = new jsPDF();          
    var elementHandler = {
      '#exportButton': function (element, renderer) {
        return true;
      }
    };
    var source = window.document.getElementsByTagName("table")[0];
    console.log(source);
    doc.fromHTML(
        source,
        15,
        15,
        {
          'width': 180,'elementHandlers': elementHandler
        }, function(bla) {
            console.log(bla);
            doc.save('details.pdf');
        });
});

$('#guarantor').multiselect({
  nonSelectedText: 'Select Guarantor',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  buttonWidth:'400px'
 });

$('#reject-loan').click( function (e) {
    e.preventDefault();
    var _this = $(this);
    var id  = $(this).data('id');
    var reason = prompt("Please enter your reason");
    if (reason == "") {
        alert("Please input a reason");
    } else {
        window.location.href = "/reject-loan/"+id+"?purpose="+reason;
    }
});