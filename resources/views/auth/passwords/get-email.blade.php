@include('layouts.header')

<section>
  <div class="shell">
    <div class="range range-sm-center">
      <div  style="margin-top: 50px" class="cell-sm-7 cell-md-5 cell-lg-4">
        <div class="block-shadow text-center">
          <div class="block-inner">
            <p class="text-uppercase text-bold text-dark">Reset Password</p>
          </div>
  <form class="form-modern form-darker" method="POST" action="{{ route('postemail') }}">
    {{ csrf_field() }}
    <div class="block-inner">
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} offset-top-22">
        <input id="get-email-form" type="email" name="email" value="{{ old('email') }}" data-constraints="@Email @Required" class="form-control">
        <label for="get-email-form" class="form-label">Email</label>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="offset-top-30 offset-sm-top-50">
      <button type="submit" class="btn btn-rect btn-primary btn-block">Reset Password</button>
    </div>
  </form>
  </div>
      </div>
    </div>
  </div>
</section>
<div style="margin-top: 250px;" class="row">
    @include('layouts.footer')
</div>
   
  </body>
</html>
