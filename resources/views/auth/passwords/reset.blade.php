@include('layouts.header')

<section>
  <div class="shell">
    <div class="range range-sm-center">
      <div  style="margin-top: 50px" class="cell-sm-7 cell-md-5 cell-lg-4">
        <div class="block-shadow text-center">
          <div class="block-inner">
            <p class="text-uppercase text-bold text-dark">Reset Password</p>
          </div>
  <form class="form-modern form-darker" method="POST" action="{{ route('password.request') }}">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">
    <div class="block-inner">
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} offset-top-22">
        <input id="get-email-form" type="email" name="email" value="{{ old('email') }}" data-constraints="@Email @Required" class="form-control">
        <label for="get-email-form" class="form-label">Email</label>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="block-inner">
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} offset-top-22">
        <input id="reset-password-form" type="password" name="password" value="{{ old('password') }}" data-constraints="@Required" class="form-control">
        <label for="reset-password-form" class="form-label">Password</label>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="block-inner">
      <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} offset-top-22">
        <input id="confirm-password-form" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" data-constraints="@Required" class="form-control">
        <label for="confirm-password-form" class="form-label">Confirm password</label>
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="offset-top-30 offset-sm-top-50">
      <button type="submit" class="btn btn-rect btn-primary btn-block">Reset Password</button>
    </div>
  </form>
  </div>
      </div>
    </div>
  </div>
</section>
<div style="margin-top: 250px;" class="row">
    @include('layouts.footer')
</div>
   
  </body>
</html>
