<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <title>Register</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arvo:400,700%7COpen+Sans:300,300italic,400,400italic,700italic,800%7CUbuntu:500">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="{{ asset('sweetalert/sweetalert.css') }}">
        <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="httpqwertywindows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
  </head>
  <body style="background-image: url(images/bg-register.jpg);" class="one-screen-page bg-gray-darker bg-image">
    <div class="page">
      <div class="page-inner">
        <header class="page-head">
          <div class="page-head-inner">
            <div class="shell text-center"><a href="/" class="brand brand-md brand-inverse"><img src="{!! load_asset('images/ecoberty_logo.jpg') !!}" alt="" width="145" height="30"/></a>
            </div>
          </div>
        </header>
        <section>
          <div class="shell">
            <div class="range range-sm-center">
              <div class="cell-sm-7 cell-md-5 cell-lg-4">
                <div class="block-shadow text-center">
                  <div class="block-inner">
                    <p class="text-uppercase text-bold text-dark">Create your account</p><br>
                    {{-- <div class="offset-top-40 offset-sm-top-60"><span class="icon icon-xl icon-gray-base material-icons-face"></span></div> --}}
                  </div>
                  <form class="form-modern form-darker" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="block-inner">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label style="margin-top: -20px" class="form-label">Name</label>
                        <input type="text" name="name" value="{{ old('name') }}" required class="form-control">
                        
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} offset-top-22">
                        <label style="margin-top: -20px" class="form-label">Email</label>
                        <input id="feedback-email" type="email" name="email" value="{{ old('email') }}" data-constraints="@Email @Required" class="form-control">
                        
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }} offset-top-22">
                        <fieldset>
                        <select id="s2" name="gender" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                            <option style="margin-top: 1000px;" value="" >Choose gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                      </fieldset>
                        @if ($errors->has('gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} offset-top-22">
                        <label style="margin-top: -20px" class="form-label">Phone Number</label>
                        <input id="phone" type="tel" name="phone" value="{{ old('phone') }}" data-constraints="@Required" class="form-control">
                        
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} offset-top-22">
                        <label style="margin-top: -20px" class="form-label">Address</label>
                        <input id="feedback-address" type="text" name="address" value="{{ old('address') }}" data-constraints="@Required" class="form-control">
                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} offset-top-22">
                        <label style="margin-top: -20px" class="form-label">Password</label>
                        <input id="register-form-password" type="password" name="password" data-constraints="@Required" class="form-control">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="form-group offset-top-22">
                        <label style="margin-top: -20px" class="form-label">Confirm Password</label>
                        <input id="register-form-password-confirm" type="password" name="password_confirmation" data-constraints="@Required" class="form-control">

                      </div>
                      <div class="offset-top-22 text-left text-secondary">
                        <label class="checkbox-inline checkbox-small">
                          <input name="input-checkbox" value="checkbox-1" type="checkbox">I agree with the&nbsp;<a href="privacy-policy.html" class="link-primary-inline">Terms and Condition</a>.
                        </label>
                      </div>
                    </div>
                    <div class="offset-top-30 offset-sm-top-40">
                      <button type="submit" class="btn btn-rect btn-primary btn-block">Sign up</button>
                    </div>
                  </form>
                </div>
                <div class="group-inline offset-top-15 text-center"><span class="text-white">Already has an account?</span><a href="/login" class="link link-primary-inverse">Sign in here.</a></div>
              </div>
            </div>
          </div>
        </section>
        <section class="page-foot">
          <div class="page-foot-inner">
            <div class="shell text-center">
              <div class="range">
                <div class="cell-xs-12">
                  <p class="rights"><span>Ecobert</span><span>&nbsp;&#169;&nbsp;</span><span id="copyright-year"></span><span>All Rights Reserved</span><br class="veil-sm"><a href="#" class="link-primary-inverse">Terms and Condition</a><span>and</span><a href="privacy-policy.html" class="link-primary-inverse">Privacy Policy</a></p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
    <div id="form-output-global" class="snackbars"></div>
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <script src="{{ asset('sweetalert/sweetalert.min.js') }}"></script>
    @include('sweet::alert')
    <script src="{!! load_asset('js/core.min.js') !!}"></script>
    <script src="{!! load_asset('js/script.js') !!}"></script>
  </body>
</html>