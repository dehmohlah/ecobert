@extends('dashboard.master')
@section('title', 'edit-users')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >

<div class="form-box-items">
    <!-- FORM BOX ITEM -->
    <div class="form-box-item">
        <h4>Edit User</h4>
        <hr class="line-separator">
        <!-- PROFILE IMAGE UPLOAD -->
        <!-- PROFILE IMAGE UPLOAD -->
        <form id="profile-info-form" method="post" action="{{ route('update-user', ['id' => $user->id])}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                <div class="form-group">
                    <figure class="user-avatar medium">
                        @if ($user->passport == '')
                        <img src="http://www.gravatar.com/avatar?d=mm&s=500" height="50" width="50" style="border-radius:25px;" alt="profile-default-image">
                        @else
                        <img src="{{ $user->avatar }}" title="avatar" alt="avatar" height="50" width="50" style="border-radius:25px;">
                        @endif
                    </figure>
                    <p class="text-header">Profile Photo</p><br>
                    <p class="upload-details"><input type="file" class="form-group" name="avatar"></p>
                </div>
                @if ($errors->has('avatar'))
                    <span class="help-block">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                @endif
            </div>
            <input type="hidden" name="avatar" value="{{ $user->avatar }}">

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
            
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email</label>
                @if (Auth::user()->role_id == 3)
                <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                @else
                <input type="text" class="form-control" name="email" value="{{ $user->email }}" disabled>
                @endif
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
          	<label for="gender">Gender</label>
                <fieldset>
                <select id="s2" name="gender" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    @if ($user->gender == 'male')
                        <option value="male" selected="selected">Male</option>
                        @else
                        <option value="male">Male</option>
                        @endif
                        @if ($user->gender == 'female')
                        <option value="female" selected="selected">Female</option>
                        @else
                        <option value="female">Female</option>
                        @endif
                </select>
              </fieldset>
                @if ($errors->has('gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                @endif
              </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address">Contact Address</label>
                <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                @if ($errors->has('address'))
                <span class="help-block">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone">Phone Number</label>
                <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>

            <div class="clearfix"></div>
            <hr class="line-separator">
            <div class="form-group">
                <button type="submit" class="btn btn-primary" style="background-color: #0f632e ! important; border: none;">
                <i class="fa fa-btn fa-user"></i> Update User
                </button>
            </div>
            <!-- /INPUT CONTAINER -->
        </form>
    </div>
</div>
@endsection