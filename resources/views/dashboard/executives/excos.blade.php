@extends('dashboard.master')
@section('title', 'list-excos page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
	<h4>List of Excos</h4>
        <hr class="line-separator">
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>Sn</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Gender</th>
			<th>Address</th>
			<th>Role</th>
			<th>Edit</th>
		</tr>
	</thead>
	<tbody>
		@if ($excos->count() > 0)
		@foreach($excos as $exco)
		<tr>
			<td>{{ $loop->index + 1 }}</td>
			<td>{{ $exco->name }}</td>
			<td>{{ $exco->email }}</td>
			<td>{{ $exco->phone }}</td>
			<td>{{ $exco->gender }}</td>
			@if ($exco->address == "")
				<td>No address giving</td>
			@else
				<td>{{ $exco->address }}</td>
			@endif
			<td>{{ ucwords($exco->role->name) }}</td>
			<td><a href="{{ route('edit-exco', ['id' => $exco->id]) }}" title="Edit {{ $exco->name }}"> <i class="glyphicon glyphicon-pencil"></i> Edit </a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<p>
	@if ($excos->count() > 0)
	{!! $excos->render() !!}
	@endif
</p>
</div>
@endsection