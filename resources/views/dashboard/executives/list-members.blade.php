@extends('dashboard.master')
@section('title', 'list-members')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
	<h4>List of Members</h4>
        <hr class="line-separator">
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>Sn</th>
			<th>Title</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone 1</th>
			<th>Gender</th>
			<th>Reg. Payment</th>
			<th>Completion</th>
			<th>Approval</th>
			<th>View More</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		@if ($members->count() > 0)
		@foreach($members as $member)
		<tr>
			<td>{{ $loop->index + 1 }}</td>
			<td>{{ $member->title }}</td>
			<td>{{ $member->last_name . " " . $member->first_name}}</td>
			<td>{{ $member->email }}</td>
			<td>{{ $member->phone_number_1 }}</td>
			<td>{{ $member->gender }}</td>
			<td>
				@if ($member->reg_payment == 0)
					Not Paid
				@else
					Paid
				@endif
			</td>
			<td>
				@if ($member->completed == 1)
					COMPLETED
				@else
					NOT COMPLETED
				@endif
			</td>
			<td>
				<select class="approve-member" id ="approve" data-id="{{ $member->id }}">
					@foreach ($status as $key => $value)
						<option value="{{ $value }}"
							@if ($member->status == $key)
								selected="selected"
							@endif
						>{{ $value }}</option>
					@endforeach
				</select>
			</td>
			<td><a href="{{ route('view-member', ['id' => $member->id]) }}" title="View complete details for {{ $member->last_name }} for better review"> <i class="glyphicon glyphicon-eye-open"></i> View </a></td>
			<td><a href="{{ route('edit-member', ['id' => $member->id]) }}" title="Edit {{ $member->last_name }}"> <i class="glyphicon glyphicon-pencil"></i> Edit </a></td>
			<td><a class="delete-member" data-id="{{ $member->id }}" href="{{ route('delete-member', ['id' => $member->id]) }}" title="Delete {{ $member->last_name }} {{ $member->first_name }}"> <i class="glyphicon glyphicon-trash Delete"></i> Delete</a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<p>
	@if ($members->count() > 0)
	{!! $members->render() !!}
	@endif
</p>

@if ($trashedMembers->count() > 0)
<h4>List of Deleted Members</h4>
        <hr class="line-separator">
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>Sn</th>
			<th>Title</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone 1</th>
			<th>Gender</th>
			<th>Restore</th>
		</tr>
	</thead>
	<tbody>
		@if ($trashedMembers->count() > 0)
		@foreach($trashedMembers as $member)
		<tr>
			<td>{{ $loop->index + 1 }}</td>
			<td>{{ $member->title }}</td>
			<td>{{ $member->last_name . " " . $member->first_name}}</td>
			<td>{{ $member->email }}</td>
			<td>{{ $member->phone_number_1 }}</td>
			<td>{{ $member->gender }}</td>
			<td><a class="restore-member" data-id="{{ $member->id }}" href="{{ route('restore-member', ['id' => $member->id]) }}" title="Restore {{ $member->last_name }} {{ $member->first_name }}"> <i class="glyphicon glyphicon-refresh Restore"></i> Restore</a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<p>
	@if ($trashedMembers->count() > 0)
	{!! $trashedMembers->render() !!}
	@endif
</p>
@endif

</div>
@endsection