@extends('dashboard.master')
@section('title', 'list-users page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
	<h4>List of Users</h4>
        <hr class="line-separator">
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>Sn</th>
			<th>Name</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Gender</th>
			<th>Address</th>
			<th>Role</th>
			<th>Edit</th>
		</tr>
	</thead>
	<tbody>
		@if ($users->count() > 0)
		@foreach($users as $user)
		<tr>
			<td>{{ $loop->index + 1 }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->phone }}</td>
			<td>{{ $user->gender }}</td>
			@if ($user->address == "")
				<td>No address giving</td>
			@else
				<td>{{ $user->address }}</td>
			@endif
			<td>{{ ucwords($user->role->name) }}</td>
			<td><a href="{{ route('edit-user', ['id' => $user->id]) }}" title="Edit {{ $user->name }}"> <i class="glyphicon glyphicon-pencil"></i> Edit </a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<p>
	@if ($users->count() > 0)
	{!! $users->render() !!}
	@endif
</p>
</div>
@endsection