@extends('dashboard.master')
@section('title', 'loan-details')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div id="member-loan-details" style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
    <a href="" class="btn btn-primary clearfix" id="exportButton"><span class="fa fa-file-pdf-o"></span> Export to PDF</a>

    <table class="table table-hover">
        <thead>
            <!-- <tr> -->
                <th><img src="{{ $member->passport }}" title="Passport" alt="avatar" height="100" width="100" style="border-radius:50px;"></th>
                <th></th>
                <th></th>
                <th></th>
            <!-- </tr> -->

        </thead>
        <tbody>
            <tr>
                <td style="float:right;"><u><h4>Personal Details</h4></u></td>
            </tr>
            <tr>
                <th>Title</th>
                <td></td>
                <td></td>
                <td>{{ $member->title }}</td>
            </tr>
            <tr>
                <th>Last Name</th>
                <td></td>
                <td></td>
                <td>{{ $member->last_name }}</td>
            </tr>
            <tr>
                <th>First Name</th>
                <td></td>
                <td></td>
                <td>{{ $member->first_name }}</td>
            </tr>
            <tr>
                <th>Gender</th>
                <td></td>
                <td></td>
                <td>{{ $member->gender }}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td></td>
                <td></td>
                <td>{{ $member->email }}</td>
            </tr>
            <tr>
                <th>Phone Number 1</th>
                <td></td>
                <td></td>
                <td>{{ $member->phone_number_1 }}</td>
            </tr>
            <tr>
                <th>Contact Address</th>
                <td></td>
                <td></td>
                <td>{{ $member->contact_address }}</td>
            </tr>
            
            <tr>
                <th style="float:right;"><u><h4>Loan Details</h4></u></th>
            </tr>

        
            <tr>
                <th>Account Name</th>
                <td></td>
                <td></td>
                <td>{{ $loan->account_name }}</td>
            </tr>
             <tr>
                <th>Account Number</th>
                <td></td>
                <td></td>
                <td>{{ $loan->account_number }}</td>
            </tr>
            <tr>
                <th>Bank</th>
                <td></td>
                <td></td>
                <td>{{ $loan->bank }}</td>
            </tr>
            <tr>
                <th>Bvn</th>
                <td></td>
                <td></td>
                <td>{{ $loan->bvn }}</td>
            </tr>
            <tr>
                <th>Loan Amount</th>
                <td></td>
                <td></td>
                <td>{{ number_format($loan->amount) }}</td>
            </tr>
            <tr>
                <th>Loan Purpose</th>
                <td></td>
                <td></td>
                <td>{{ $loan->purpose }}</td>
            </tr>
            <tr>
                <th>Loan Status</th>
                <td></td>
                <td></td>
                <td>{{ $loan->status }}</td>
            </tr>


            <tr>
                <th style="float:right;"><u><h4>Guarantors Details</h4></u></th>
            </tr>

            @foreach($guarantors as $guarantor)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                </tr>
                <tr>
                    <th>Guarantor's Name</th>
                    <td></td>
                    <td></td>
                    <td>{{ $guarantor->name }}</td>
                </tr>
                <tr>
                    <th>Guarantor's Email</th>
                    <td></td>
                    <td></td>
                    <td>{{ $guarantor->email }}</td>
                </tr>
                <tr>
                    <th>Guarantor's Phone Number</th>
                    <td></td>
                    <td></td>
                    <td>{{ $guarantor->phone_number }}</td>
                </tr>
                <tr>
                    <th>Guarantor's Status</th>
                    <td></td>
                    <td></td>
                    @if ($guarantor->status == 0)
                        <td>Pending</td>
                    @elseif ($guarantor->status == 1)
                        <td>Accepted</td>
                    @else
                        <td>Rejected</td>
                    @endif
                </tr>
            @endforeach

            <tr>
                <th style="float:right;"><u><h4>Contribution Details</h4></u></th>
            </tr>
            @foreach($contributions as $contribution)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                </tr>
                <tr>
                    <th>Amount</th>
                    <td></td>
                    <td></td>
                    <td>{{ number_format($contribution->amount) }}</td>
                </tr>
                <tr>
                    <th>Date Paid</th>
                    <td></td>
                    <td></td>
                    <td>{{ Carbon\Carbon::createFromTimeStamp(strtotime($loan->created_at))->toDayDateTimeString() }}</td>
                </tr>
            @endforeach
                <tr>
                    <th>Total Savings</th>
                    <td></td>
                    <td></td>
                    <td>{{ number_format($savings) }}</td>
                </tr>

            <tr>
                <th style="float:right;"><u><h4>Official Use</h4></u></th>
            </tr>

            <tr>
                <th>Status</th>
                <td></td>
                <td></td>
                <td>{{ $loan->status }}</td>
            </tr>
            <tr>
                <th>Date Registered for loan</th>
                <td></td>
                <td></td>
                <td>{{ Carbon\Carbon::createFromTimeStamp(strtotime($loan->created_at))->diffForHumans() }}</td>
            </tr>
        </tbody>
    </table>
    <a href="{{ route('accept-loan', ['id' => $loan->id]) }}" class="btn btn-primary clearfix" ><span></span> Accept Loan Application</a>
    <a href="" id="reject-loan" data-id="{{ $loan->id }}" class="btn btn-primary clearfix" ><span></span> Reject Loan Application</a>
</div>
@endsection
<!-- you need to include the shieldui css and js assets in order for the components to work -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.js"></script> -->
<!-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script> -->

<!--<script type="text/javascript">

</script> -->