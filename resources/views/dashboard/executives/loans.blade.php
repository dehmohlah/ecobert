@extends('dashboard.master')
@section('title', 'list-loans page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
	<h4>Loan Applications</h4>
        <hr class="line-separator">
<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>Sn</th>
			<th>Name</th>
			<th>Email</th>
			<th>Amount</th>
			<th>BVN</th>
			<th>Account Name</th>
			<th>Account Number</th>
			<th>Bank</th>
			<th>Status</th>
			<th>View More</th>
		</tr>
	</thead>
	<tbody>
		@if ($loans->count() > 0)
		@foreach($loans as $loan)
		<tr>
			<td>{{ $loop->index + 1 }}</td>
			<td>{{ $loan->name }}</td>
			<td>{{ $loan->email }}</td>
			<td>{{ $loan->amount }}</td>
			<td>{{ $loan->bvn }}</td>
			<td>{{ $loan->account_name }}</td>
			<td>{{ $loan->account_number }}</td>
			<td>{{ $loan->bank }}</td>
			<td>{{ $loan->status }}</td>
			<td><a href="{{ route('view-loan', ['id' => $loan->id]) }}" title="View complete details for {{ $loan->last_name }} for better review"> <i class="glyphicon glyphicon-eye-open"></i> View </a></td>
		</tr>
		@endforeach
		@endif
	</tbody>
</table>
<p>
	@if ($loans->count() > 0)
	{!! $loans->render() !!}
	@endif
</p>
</div>
@endsection