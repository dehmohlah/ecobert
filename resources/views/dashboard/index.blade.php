@extends('dashboard.master')
@section('title', 'Index page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
    <div class="row" style="margin-left: 1%;">
        @can ('not-approved-member', isset(Auth::user()->member) ? Auth::user()->member->status : "" )
            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-cloud-upload fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="padding-left: 30px;">
                            <h6 class="text-uppercase"><strong>Your membership is still under review</strong></h6>
                            <p><span>Note that your membership will <b>NOT</b> be approved if you are yet to pay the registration fee of <b>2500 naira</b></span></p>
                        </div>
                    </div>
                </div>
            </div>
        @endcan
        @can ('not-paid-reg-fee', isset(Auth::user()->member) ? Auth::user()->member->status : "")
            <a href="/reg/payment-form"><div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate" >
                            <i class="fa fa-registered fa-10x" style="padding: 30px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="padding-left: 40px;">
                            <h6 class="text-uppercase">Make registration payment here</h6>
                        </div>
                    </div>

                </div>
            </div></a>
        @endcan
        @can ('paid-reg-fee', isset(Auth::user()->member) ? Auth::user()->member->status : "")
       <!--  <a href="/contribution/payment-form"><div class="col-lg-3 col-md-6">
            <div class="card card-inverse card-success">
                <div class="card-block bg-success">
                    <div class="rotate" >
                        <i class="fa fa-credit-card fa-10x" style="padding: 30px; font-size: 5rem"></i>
                    </div>
                    <div class="user-stat" style="padding-left: 40px;">
                        <h6 class="text-uppercase">Make Contributions</h6>
                        <h1 class="display-1" style="padding-left: 30px;"></h1>
                    </div>
                </div>

            </div>
        </div></a> -->
        @endcan

        @can ('exco-user', Auth::user()->role_id )
            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-thumbs-down fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="">
                            <h6 class="text-uppercase" >Unapproved Applications</h6>
                            <h1 class="" style="">{{ $totalUnapprovedMember }}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-thumbs-down fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="">
                            <h6 class="text-uppercase" >Approved Applications</h6>
                            <h1 class="" style="">{{ $totalApprovedMember }}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-thumbs-down fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="">
                            <h6 class="text-uppercase" >Pending Applications</h6>
                            <h1 class="" style="">{{ $totalPendingMember }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-users fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="">
                            <h6 class="text-uppercase" >Total No of Users</h6>
                            <h1 class="" style="">{{ $totalUser }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-user-secret fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="padding-left: 40px;">
                            <h6 class="text-uppercase" >Total No Of members</h6>
                            <h1 class="" style="">{{ $totalMember }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-inverse card-success">
                <div class="card-block bg-success">
                    <div class="rotate">
                        <i class="fa fa-credit-card fa-10x" style="padding: 50px; font-size: 5rem"></i>
                    </div>
                    <div class="user-stat" style="">
                        <h6 class="text-uppercase" >Total Ecoberty Contributions</h6>
                        <h1 class="" style="">{{ number_format($totalEcobertyContribution) }}</h1>
                    </div>
                </div>
            </div>
        @endcan


        <!-- <div class="col-lg-3 col-md-6">
            <div class="card card-inverse card-success">
                <div class="card-block bg-success">
                    <div class="rotate">
                        <i class="fa fa-video-camera fa-10x" style="padding: 50px; font-size: 5rem"></i>
                    </div>
                    <div class="user-stat" style="">
                        <h6 class="text-uppercase" >Number Of users</h6>
                        <h1 class="display-1" style=""></h1>
                    </div>
                </div>
            </div>
        </div> -->
        @can ('members', Auth::user()->role_id )
            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-user-secret fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="padding-left: 40px;">
                            <h6 class="text-uppercase" >Total No Of Referrer</h6>
                            <h1 class="" style="">{{ $totalReferer }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-user-secret fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="padding-left: 40px;">
                            <h6 class="text-uppercase" >Referred By:</h6>
                            <h3 class="" style="">{{ Auth::user()->member->introduced_by }}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div style="color: #0f632e ! important; border: none;" class="">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-credit-card fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="" style="">
                            <h6 class="text-uppercase" >Your Total Contributions</h6>
                            <h1 class="" style="">{{ number_format($totalContribution) }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        <!-- <div class="col-lg-3 col-md-6">
            <div class="card card-inverse card-success">
                <div class="card-block bg-success">
                    <div class="rotate">
                        <i class="fa fa-video-camera fa-10x" style="padding: 50px; font-size: 5rem"></i>
                    </div>
                    <div class="user-stat" style="padding-left: 40px;">
                        <h6 class="text-uppercase" >Amount you can borrow</h6>
                        <h1 class="display-1" style="padding-left: 30px;"></h1>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
@endsection