@extends('dashboard.master')
@section('title', 'edit-users')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >

<div class="form-box-items">
    <!-- FORM BOX ITEM -->
    <div class="form-box-item">
        <h4>Loan Form</h4>
        <hr class="line-separator">
        <!-- PROFILE IMAGE UPLOAD -->
        <!-- PROFILE IMAGE UPLOAD -->
        <form id="profile-info-form" method="post" action="{{ route('apply-loan') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label for="passport">Passport</label>
                    <figure class="user-avatar medium">
                        @if ($member->passport == '')
                        <img src="http://www.gravatar.com/avatar?d=mm&s=500" height="50" width="50" style="border-radius:25px;" alt="profile-default-image">
                        @else
                        <img src="{{ $member->passport }}" title="avatar" alt="avatar" height="100" width="100" style="">
                        @endif
                    </figure>
                    
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $member->title}} {{ $member->last_name}} {{ $member->first_name}}" disabled="disabled">
            </div>
            <input type="hidden" class="form-control" name="member_id" value="{{ $member->id }}" />
            <input type="hidden" class="form-control" name="email" value="{{ $member->email }}" />
            <input type="hidden" class="form-control" name="name" value="{{ $member->title}} {{ $member->last_name}} {{ $member->first_name}}" />
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value="{{ $member->email }}" disabled>
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
          	 <label for="gender">Gender</label>
             <input type="text" class="form-control" name="gender" value="{{ $member->gender }}" disabled>   
            </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address">Contact Address</label>
                <input type="text" class="form-control" name="address" value="{{ $member->contact_address }}" disabled>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone">Phone Number</label>
                <input type="text" class="form-control" name="phone" value="{{ $member->phone_number_1 }}" disabled>
            </div>

            <div class="form-group{{ $errors->has('bvn') ? ' has-error' : '' }}">
                <label for="bvn">BVN</label>
                <input type="text" class="form-control" value="{{ old('bvn') }}" name="bvn" >
                @if ($errors->has('bvn'))
                <span class="help-block">
                    <strong>{{ $errors->first('bvn') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                <label for="amount">Amount</label>
                <input type="text" class="form-control" value="{{ old('amount') }}" name="amount" >
                @if ($errors->has('amount'))
                <span class="help-block">
                    <strong>{{ $errors->first('amount') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('account_name') ? ' has-error' : '' }}">
                <label for="account_name">Account Name</label>
                <input type="text" class="form-control" value="{{ old('account_name') }}" name="account_name" >
                @if ($errors->has('account_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('account_name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
                <label for="account_number">Account Number</label>
                <input type="text" class="form-control" value="{{ old('account_number') }}" name="account_number" >
                @if ($errors->has('account_number'))
                <span class="help-block">
                    <strong>{{ $errors->first('account_number') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
                <label for="bank">Bank</label>
                <input type="text" class="form-control" value="{{ old('bank') }}" name="bank" >
                @if ($errors->has('bank'))
                <span class="help-block">
                    <strong>{{ $errors->first('bank') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('guarantor') ? ' has-error' : '' }}">
                <label for="guarantor">Select guarantors</label>
                <br>
                 <select id="guarantor" name="guarantor[]" multiple class="form-control select-filter" >
                  @foreach ($members as $member)
                    <option value="{{ $member->id }}">
                        {{ $member->title }} {{ $member->last_name }} {{ $member->first_name }}
                    </option>
                  @endforeach
                 </select>
                 <br>
                @if ($errors->has('guarantor'))
                    <span class="help-block">
                        <strong>{{ $errors->first('guarantor') }}</strong>
                    </span>
                @endif
              </div>




            <div class="form-group{{ $errors->has('purpose') ? ' has-error' : '' }}">
                <label for="purpose">Purpose of Loan</label>
                <input type="text" class="form-control" value="{{ old('purpose') }}" name="purpose" >
                @if ($errors->has('purpose'))
                <span class="help-block">
                    <strong>{{ $errors->first('purpose') }}</strong>
                </span>
                @endif
            </div>

            
            <label for="TandC">Notice</label>
            <div class="form-group{{ $errors->has('purpose') ? ' has-error' : '' }}">
            <textarea class="form-control" disabled>Please note that this loan is going to be a shot term loan of 10 months. Our interest rate is 5%. Before we can grant you a loan, you must provide multiple guarantors. </textarea> 
            </div>

            <div class="clearfix"></div>
            <hr class="line-separator">
            <div class="form-group">
                <button type="submit" class="btn btn-primary" style="background-color: #0f632e ! important; border: none;">
                <i class="fa fa-btn fa-user"></i> Apply
                </button>
            </div>
            <!-- /INPUT CONTAINER -->
        </form>
    </div>
</div>
 
@endsection