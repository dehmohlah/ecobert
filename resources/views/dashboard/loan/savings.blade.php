@extends('dashboard.master')
@section('title', 'Index page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>


<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
    <div class="row" style="margin-left: 1%;">
        	
    		<div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate">
                            <i class="fa fa-thumbs-down fa-10x" style="padding: 50px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="">
                            <h6 class="text-uppercase" >Savings</h6>
                            <h1 class="" style="">₦{{ number_format($savings) }}</h1>
                        </div>
                    </div>
                </div>
            </div>


            <a href="{{ route('loan-apply-form') }}"><div class="col-lg-3 col-md-6">
                <div class="card card-inverse card-success">
                    <div class="card-block bg-success">
                        <div class="rotate" >
                            <i class="fa fa-registered fa-10x" style="padding: 30px; font-size: 5rem"></i>
                        </div>
                        <div class="user-stat" style="padding-left: 40px;">
                            <h6 class="text-uppercase">Apply here</h6>
                        </div>
                    </div>

                </div>
            </div></a>

    </div>
</div>
@endsection