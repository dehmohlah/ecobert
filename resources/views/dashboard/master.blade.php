<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="Ecoberty" content="Welcome to Ecoberty Multipurpose Cooperative Society. Our vision is to be the most supportive and responsive cooperative society in Africa" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- bootstrap -->
        <link href="{!! load_asset('dashboard-styles/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' media="all" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- //bootstrap -->
        <link href="{!! load_asset('dashboard-styles/css/dashboard.css') !!}" rel="stylesheet">
        <!-- Custom Theme files -->
        <link href="{!! load_asset('dashboard-styles/css/style.css') !!}" rel='stylesheet' type='text/css' media="all" />
        <!--start-smoth-scrolling-->
        <!-- fonts -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
        <!-- //fonts -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{!! load_asset('images/logo.png') !!}" rel="stylesheet">
        <link rel="shortcut icon" href="{!! load_asset('/images/favicon.ico') !!}">
        <!-- Font Awesome CSS -->
        <link rel="stylesheet" href="{!! load_asset('font-awesome/css/font-awesome.min.css') !!}">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
        <!-- Devicons CSS  -->
        <link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/master/devicon.min.css">
        <link href="{!! load_asset('devicons-master/css/devicons.min.css') !!}"
            
            <!-- Custom Theme files -->
            <link href="{!! load_asset ('sweetalert/sweetalert.css') !!}" rel="stylesheet"/>
            <!--start-smoth-scrolling-->
            <!-- fonts -->
            <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
            <link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
        </head>
        <body>
            @yield('content')
            <!-- Bootstrap core JavaScript
            ================================================== -->
            <script src="{!! load_asset('sweetalert/sweetalert.min.js') !!}"></script>

            @include('sweet::alert')
            
            <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
            {{-- <script src="{!! load_asset('dashboard-styles/js/jquery-1.11.1.min.js') !!}"></script> --}}
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
            <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
            <!-- // <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script> -->
            <!-- // <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script> -->
            <script src="{!! load_asset('dashboard-styles/js/bootstrap.min.js') !!}"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<script src="{!! load_asset('sweetalert/sweetalert.min.js') !!}"></script>
            <script src="{!! load_asset('dashboard-styles/js/app.js') !!}"></script>
            


            <script type="text/javascript">
                $(document).ready(function() {
                    $('.top-navigation').on("click", function(){
                        $('.drop-navigation').toggle('slow');
                    });
                });
            </script>
        </body>
    </html>