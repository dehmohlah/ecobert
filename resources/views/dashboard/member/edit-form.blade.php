@extends('dashboard.master')
@section('title', 'edit-members')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >

<div class="form-box-items">
    <!-- FORM BOX ITEM -->
    <div class="form-box-item">
        <h4>Edit Member</h4>
        <hr class="line-separator">
        <!-- PROFILE IMAGE UPLOAD -->
        <!-- PROFILE IMAGE UPLOAD -->
        <form id="profile-info-form" method="post" action="{{ route('update-member', ['id' => $member->id])}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                <div class="form-group">
                    <figure class="user-avatar medium">
                        @if ($member->passport == '')
                        <img src="http://www.gravatar.com/avatar?d=mm&s=500" alt="profile-default-image">
                        @else
                        <img src="{{ $member->passport }}" title="avatar" alt="avatar" height="50" width="50" style="border-radius:25px;">
                        @endif
                    </figure>
                    <p class="text-header">Profile Photo</p><br>
                    <p class="upload-details"><input type="file" class="form-group" name="avatar"></p>
                </div>
                @if ($errors->has('avatar'))
                    <span class="help-block">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                @endif
            </div>
            <input type="hidden" name="passport" value="{{ $member->passport }}">
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
          	<label for="title">Title</label>
                <fieldset>
                <select id="s2" name="title" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    @if ($member->title == 'mr')
                        <option value="mr" selected="selected">Mr</option>
                        @else
                        <option value="mr">Mr</option>
                        @endif
                        @if ($member->title == 'mrs')
                        <option value="mrs" selected="selected">Mrs</option>
                        @else
                        <option value="mrs">Mrs</option>
                        @endif
                        @if ($member->title == 'miss')
                        <option value="miss" selected="selected">Miss</option>
                        @else
                        <option value="miss">Miss</option>
                        @endif
                        @if ($member->title == 'others')
                        <option value="others" selected="selected">Others</option>
                        @else
                        <option value="others">Other</option>
                    @endif
                </select>
              </fieldset>
                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
              </div>

            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="last_name">Last name</label>
                <input type="text" class="form-control" name="last_name" value="{{ $member->last_name }}">
                @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first_name">First name</label>
                <input type="text" class="form-control" name="first_name" value="{{ $member->first_name }}">
                @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
                @endif
            </div>
            
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email</label>
                @if (Auth::user()->role_id == 3)
                <input type="text" class="form-control" name="email" value="{{ $member->email }}">
                @else
                <input type="text" class="form-control" name="email" value="{{ $member->email }}" disabled>
                @endif
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>



          <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
          	<label for="status">Status</label>
                <fieldset>
                <select id="s2" name="status" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    @if ($member->status == 0)
                    	<option value="0" selected="selected">Not Approve</option>
                    @else
                    	<option value="0">Not Approve</option>
                    @endif
                    @if ($member->status == 1)
                        <option value="1" selected="selected">Dissapprove</option>
                    @else
                        <option value="1">Dissapprove</option>
                    @endif

                    @if ($member->status == 2)
                        <option value="2" selected="selected">Approve</option>
                    @else
                        <option value="2">Approve</option>
                    @endif
                </select>
              </fieldset>
                @if ($errors->has('status'))
                    <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
          	<label for="gender">Gender</label>
                <fieldset>
                <select id="s2" name="gender" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    @if ($member->gender == 'male')
                        <option value="male" selected="selected">Male</option>
                        @else
                        <option value="male">Male</option>
                        @endif
                        @if ($member->gender == 'female')
                        <option value="female" selected="selected">Female</option>
                        @else
                        <option value="female">Female</option>
                        @endif
                </select>
              </fieldset>
                @if ($errors->has('gender'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                @endif
              </div>

            <div class="form-group{{ $errors->has('contact_address') ? ' has-error' : '' }}">
                <label for="contact_address">Contact Address</label>
                <input type="text" class="form-control" name="contact_address" value="{{ $member->contact_address }}">
                @if ($errors->has('contact_address'))
                <span class="help-block">
                    <strong>{{ $errors->first('contact_address') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('postal_address') ? ' has-error' : '' }}">
                <label for="postal_address">Postal Address</label>
                <input type="text" class="form-control" name="postal_address" value="{{ $member->postal_address }}">
                @if ($errors->has('postal_address'))
                <span class="help-block">
                    <strong>{{ $errors->first('postal_address') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('phone_number_1') ? ' has-error' : '' }}">
                <label for="phone_number_1">Phone Number 1</label>
                <input type="text" class="form-control" name="phone_number_1" value="{{ $member->phone_number_1 }}">
                @if ($errors->has('phone_number_1'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone_number_1') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('phone_number_2') ? ' has-error' : '' }}">
                <label for="phone_number_2">Phone Number 2</label>
                <input type="text" class="form-control" name="phone_number_2" value="{{ $member->phone_number_2 }}">
                @if ($errors->has('phone_number_2'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone_number_2') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                <label for="date_of_birth">Date of Birth</label>
                <input type="text" class="form-control" name="date_of_birth" value="{{ $member->date_of_birth }}">
                @if ($errors->has('date_of_birth'))
                <span class="help-block">
                    <strong>{{ $errors->first('date_of_birth') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
                <label for="place_of_birth">Place of Birth</label>
                <input type="text" class="form-control" name="place_of_birth" value="{{ $member->place_of_birth }}">
                @if ($errors->has('place_of_birth'))
                <span class="help-block">
                    <strong>{{ $errors->first('place_of_birth') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('monthly_contribution') ? ' has-error' : '' }}">
          	  <label for="monthly_contribution">Monthly Contribution</label>
                <fieldset>
                <select id="s2" name="monthly_contribution" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    @if ($member->monthly_contribution == 1000)
                    	<option value="1000" selected>1,000</option>
                    @else
                    	<option value="1000">1,000</option>
                    @endif
                    @if ($member->monthly_contribution == 3000)
                    	<option value="3000" selected>3,000</option>
                    @else
                    	<option value="3000">3,000</option>
                    @endif

                    @if ($member->monthly_contribution == 5000)
                    	<option value="5000" selected>5,000</option>
                    @else
                    	<option value="5000">5,000</option>
                    @endif

                    @if ($member->monthly_contribution == 10000)
                    	<option value="10000" selected>10,000</option>
                    @else
                    	<option value="10000">10,000</option>
                    @endif

                    @if ($member->monthly_contribution == 10000)
                    	<option value="10000" selected>10,000</option>
                    @else
                    	<option value="20000">20,000</option>
                    @endif
                    
                    @if ($member->monthly_contribution == 300000)
                    	<option value="30000" selected>30,000</option>
                    @else
                    	<option value="30000">30,000</option>
                    @endif

                    @if ($member->monthly_contribution == 300000)
                    	<option value="40000" selected>40,000</option>
                    @else
                    	<option value="40000">40,000</option>
                    @endif

                    @if ($member->monthly_contribution == 300000)
                    	<option value="50000" selected>50,000</option>
                    @else
                    	<option value="50000">50,000</option>
                    @endif

                    @if ($member->monthly_contribution == 300000)
                    	<option value="others" selected>Others</option>
                    @else
                    	<option value="others">Others</option>
                    @endif
                </select>
              </fieldset>
                @if ($errors->has('monthly_contribution'))
                    <span class="help-block">
                        <strong>{{ $errors->first('monthly_contribution') }}</strong>
                    </span>
                @endif
              </div>
            <!-- INPUT CONTAINER -->
            <div class="clearfix"></div>
            <hr class="line-separator">
            <div class="form-group">
                <button type="submit" class="btn btn-primary"style="background-color: #0f632e ! important; border: none;">
                <i class="fa fa-btn fa-user"></i> Update Member
                </button>
            </div>
            <!-- /INPUT CONTAINER -->
        </form>
    </div>
</div>
@endsection