@extends('dashboard.master')
@section('title', 'list-members')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div id="member-details" style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
    <a href="" class="btn btn-primary clearfix" id="exportButton"><span class="fa fa-file-pdf-o"></span> Export to PDF</a>

    <table class="table table-hover">
        <thead>
            <!-- <tr> -->
                <th><img src="{{ $member->passport }}" title="Passport" alt="avatar" height="100" width="100" style="border-radius:50px;"></th>
                <th></th>
                <th></th>
                <th></th>
                <th><img src="{{ $member->signature }}" title="Signature" alt="signature" height="100" width="100" style="border-radius:50px;"></th>
            <!-- </tr> -->

        </thead>
        <tbody>
            <tr>
                <td style="float:right;"><u><h4>Personal Details</h4></u></td>
            </tr>
            <tr>
                <th>Title</th>
                <td></td>
                <td></td>
                <td>{{ $member->title }}</td>
            </tr>
            <tr>
                <th>Last Name</th>
                <td></td>
                <td></td>
                <td>{{ $member->last_name }}</td>
            </tr>
            <tr>
                <th>First Name</th>
                <td></td>
                <td></td>
                <td>{{ $member->first_name }}</td>
            </tr>
            <tr>
                <th>Gender</th>
                <td></td>
                <td></td>
                <td>{{ $member->gender }}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td></td>
                <td></td>
                <td>{{ $member->email }}</td>
            </tr>
            <tr>
                <th>Phone Number 1</th>
                <td></td>
                <td></td>
                <td>{{ $member->phone_number_1 }}</td>
            </tr>
            <tr>
                <th>Phone Number 2</th>
                <td></td>
                <td></td>
                <td>{{ $member->phone_number_2 }}</td>
            </tr>
            <tr>
                <th>Contact Address</th>
                <td></td>
                <td></td>
                <td>{{ $member->contact_address }}</td>
            </tr>
            <tr>
                <th>Postal Address</th>
                <td></td>
                <td></td>
                <td>{{ $member->postal_address }}</td>
            </tr>
            <tr>
                <th>Date Of Birth</th>
                <td></td>
                <td></td>
                <td>{{ $member->date_of_birth }}</td>
            </tr>
            <tr>
                <th>Place Of Birth</th>
                <td></td>
                <td></td>
                <td>{{ $member->place_of_birth }}</td>
            </tr>
            <tr>
                <th>Nationality</th>
                <td></td>
                <td></td>
                <td>{{ $member->nationality }}</td>
            </tr>
            <tr>
                <th>Home Town/Village Of Origin</th>
                <td></td>
                <td></td>
                <td>{{ $member->home_town_village_of_origin }}</td>
            </tr>
            <tr>
                <th>Local Government Area</th>
                <td></td>
                <td></td>
                <td>{{ $member->local_government_area }}</td>
            </tr>
            <tr>
                <th>Monthly Contribution</th>
                <td></td>
                <td></td>
                <td>{{ $member->monthly_contribution }}</td>
            </tr>
            <tr>
                <th style="float:right;"><u><h4>Next Of Kin Details</h4></u></th>
            </tr>
            <tr>
                <th>Name Of Next Of Kin</th>
                <td></td>
                <td></td>
                <td>{{ $member->name_of_next_of_kin }}</td>
            </tr>
            <tr>
                <th>Address Of Next Of Kin</th>
                <td></td>
                <td></td>
                <td>{{ $member->address_of_next_of_kin }}</td>
            </tr>
            <tr>
                <th>Phone Of Next Of Kin</th>
                <td></td>
                <td></td>
                <td>{{ $member->phone_of_next_of_kin }}</td>
            </tr>
            <tr>
                <th style="float:right;"><u><h4>Place of work Details</h4></u></th>
            </tr>
            <tr>
                <th>Company address</th>
                <td></td>
                <td></td>
                <td>{{ $member->company_address }}</td>
            </tr>
             <tr>
                <th>Date Joined Company</th>
                <td></td>
                <td></td>
                <td>{{ $member->date_join_company }}</td>
            </tr>
            <tr>
                <th>Post</th>
                <td></td>
                <td></td>
                <td>{{ $member->post }}</td>
            </tr>
            <tr>
                <th>Responsibilities</th>
                <td></td>
                <td></td>
                <td>{{ $member->responsibilities }}</td>
            </tr>
            <tr>
                <th style="float:right;"><u><h4>Introducer Details</h4></u></th>
            </tr>
            <tr>
                <th>Introducer Name</th>
                <td></td>
                <td></td>
                <td>{{ $member->introduced_by }}</td>
            </tr>
            <tr>
                <th>Introducer Number</th>
                <td></td>
                <td></td>
                <td>{{ $member->introducer_number }}</td>
            </tr>
            <tr>
                <th>Introducer Number</th>
                <td></td>
                <td></td>
                <td>{{ $member->introducer_number }}</td>
            </tr>

            <tr>
                <th style="float:right;"><u><h4>Oficial use</h4></u></th>
            </tr>
            <tr>
                <th>Completion</th>
                <td></td>
                <td></td>
                @if ($member->completed == 1)
                <td>COMPLETED</td>
                @else
                <td>NOT COMPLETED</td>
                @endif
            </tr>
            <tr>
                <th>Registration fee</th>
                <td></td>
                <td></td>
                @if ($member->reg_payment == 1)
                <td>PAID</td>
                @else
                <td>NOT PAID</td>
                @endif
            </tr>
            <tr>
                <th>Status</th>
                <td></td>
                <td></td>
                @if ($member->status == 1)
                <td>APPROVED</td>
                @else
                <td>NOT APPROVED</td>
                @endif
            </tr>
            <tr>
                <th>Date Registered</th>
                <td></td>
                <td></td>
                <td>{{ Carbon\Carbon::createFromTimeStamp(strtotime($member->created_at))->diffForHumans() }}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection
<!-- you need to include the shieldui css and js assets in order for the components to work -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.js"></script> -->
<!-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script> -->

<!--<script type="text/javascript">

</script> -->