<div class="col-sm-3 col-md-2 sidebar">
    <div class="top-navigation">
        <div class="t-menu" style="margin-left: 10%;">MENU</div>
        
        <div class="clearfix"> </div>
    </div>

    <div class="drop-navigation drop-navigation">
        <ul class="nav nav-sidebar" style="margin-top: 20%;">
            <li><a href="{{ route('dashboard') }}" class="active-page"><i class="fa fa-home" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Home</a>
            </li>
            @can ('paid-reg-fee', isset(Auth::user()->member) ? Auth::user()->member->status : "" )
            <li><a href="/contribution/payment-form"><i class="fa fa-credit-card" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Make Contributions</a>
            </li>
            <li><a href="{{ route('loan-apply') }}"><i class="fa fa-product-hunt" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Loan Application</a>
            </li>
            <li><a href="/edit-member/{{ Auth::user()->id }}"><i class="fa fa-edit" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Edit Membership data</a>
            </li>
            @endcan 
            @can ('not-paid-reg-fee', isset(Auth::user()->member) ? Auth::user()->member->status: "")
            <li><a href="/reg/payment-form"><i class="fa fa-registered" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Reg. Payment</a>
            </li>
            <li><a href="/edit-member/{{ Auth::user()->id }}"><i class="fa fa-edit" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Edit Membership data</a>
            </li>
            @endcan
            @can ('exco-user', Auth::user()->role_id )
            <li><a href="{{ route('members') }}"><i class="fa fa-user-secret" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Members</a>
            </li>
            <li><a href="{{ route('unapproved-members') }}"><i class="fa fa-plus-circle" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Unapproved Members</a>
            </li>
            <li><a href="{{ route('users') }}"><i class="fa far fa-users" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Users</a>
            </li>
            <li><a href="{{ route('excos') }}"><i class="fa fa-star" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Executives</a>
            </li>
            <li><a href="/reg/manual-payment"><i class="fa fa-registered" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Record Manual reg.</a>
            </li>
            <li><a href="/contribution/manual-payment"><i class="fa fa-plus-circle" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Record Manual Contrib.</a>
            </li>
            <li><a href="{{ route('loan-applications') }}"><i class="fa fa-user-secret" aria-hidden="true" style="margin: 0 0.5em 0 0;"></i>Loan Application</a>
            </li>
            @endcan
            
        </ul>
        <!-- script-for-menu -->
        <div class="side-bottom">
            <div class="copyright">
                <strong style="color: #fff"> #Ecoberty {{ \Carbon\Carbon::now()->year }}.</strong>
                <strong style="color: #fff">Made with <i class="fa fa-heart" style="color:red;"></i>
                <span class="incognito-text">By</span> <a href="https://github.com/andela-araimi" target="_blank">Ecoberty</a></strong>
            </div>
        </div>
    </div>
</div>
