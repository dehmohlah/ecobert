<header class="clearfix" style="background-color: #eee;">
    <!-- Start  Logo & Naviagtion  -->
    <div style="margin-top: -8px;" class="navbar navbar-default navbar-fixed-top">
        <div style="margin-top: 20px;" class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a href="/" class="rd-navbar-brand brand"><img class="logo-customize" src="{!! load_asset('images/ecoberty_logo.jpg') !!}" alt="" width="200" height="80"/></a>
        </div>
        <div class="navbar-collapse collapse">
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
                
                <!-- End Navigation List -->
                <li id="dropdown" style="margin-top: 8%; color: #6b6b6b;">
                    @if (Auth::user()->avatar == "")
                        <a class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"> {{ Auth::user()->name }} <img src="http://www.gravatar.com/avatar?d=mm&s=500" class="img-circle" height="50" width="50" style="border-radius:25px;" /><i class="fa fa-caret-down" aria-hidden="true">
                            </i>
                        </a>
                    @else
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false"> {{ Auth::user()->name }} <img src="{{ Auth::user()->avatar }}" class="img-circle" height="50" width="50" style="border-radius:25px;" /><i class="fa fa-caret-down" aria-hidden="true">
                        </i>
                    </a>
                    @endif
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2" style="margin-left: 20%;">
                        <li>
                            <a href="{{ route('edit-profile') }}">
                                <i class="fa fa-btn fa-user"></i> {{ ucwords(Auth::user()->name) }}'s profile
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('changepassword') }}">
                                <i class="fa fa-unlock-alt" aria-hidden="true"></i> Change Password
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a class="dropdown-item" href="{{ route('dashboard') }}"> <i class="fa fa-btn fa-dashboard" style="margin: 0 0.5em 0 0;"></i>Dashboard</a>
                        <li><a class="dropdown-item" href="{{ route('logout') }}" > <i class="fa fa-btn fa-power-off" style="margin: 0 0.5em 0 0;"></i>Logout</a></li>
                    </ul>
                </li>
            </ul>
            <!--End Navigation List-->
        </div>
    </div>
    <!--End Header Logo & Naviagtion-->
</header>