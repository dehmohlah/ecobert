@extends('dashboard.master')
@section('title', 'Index page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>

  
<form style="margin-top:50px;" method="POST" action="{{ route('manual-record') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row" style="margin-bottom:40px;">
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                  @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}">
                  <label for="transaction">Transaction Id</label>
                  <input type="text" class="form-control" name="id" value="{{ old('id') }}">
                  @if ($errors->has('id'))
                  <span class="help-block">
                      <strong>{{ $errors->first('id') }}</strong>
                  </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('reference') ? ' has-error' : '' }}">
                  <label for="transaction-ref">Transaction Ref</label>
                  <input type="text" class="form-control" name="reference" value="{{ old('reference') }}">
                  @if ($errors->has('reference'))
                  <span class="help-block">
                      <strong>{{ $errors->first('reference') }}</strong>
                  </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('transaction_date') ? ' has-error' : '' }}">
                  <label for="transaction_date">Transaction Date</label>
                  <input type="date" class="form-control" name="transaction_date" value="{{ old('transaction_date') }}">
                  @if ($errors->has('transaction_date'))
                  <span class="help-block">
                      <strong>{{ $errors->first('transaction_date') }}</strong>
                  </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('mode_of_payment') ? ' has-error' : '' }}">
              <fieldset>
                <label for="name">Mode of payment</label>
              <select name="mode_of_payment" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                  <option style="margin-top: 1000px;" value="{{ old('mode_of_payment') }}" ></option>
                  <option value="Bank Transfer">Transfer</option>
                  <option value="Bank Deposit">Deposit</option>
                  <option value="Money To Hand">Money to hand</option>
                  <option value="Others">Other</option>
              </select>
            </fieldset>
            @if ($errors->has('mode_of_payment'))
              <span class="help-block">
                  <strong>{{ $errors->first('mode_of_payment') }}</strong>
              </span>
            @endif
            </div>
            @if ($purpose == "Registration Fee")
              <div class="form-group{{ $errors->has('reg') ? ' has-error' : '' }}">
                <label for="name">{{ $purpose }}</label>
                <input type="text" class="form-control" name="reg" value="₦ 2,500" required="required" disabled>
              </div>
              <input type="hidden" name="purpose" value="registration-fee" >
            @else
              <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                <fieldset>
                  <label for="name">Contribution</label>
                <select name="amount" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    <option style="margin-top: 1000px;" value="{{ old('amount') }}" ></option>
                    <option value="100000">₦ 1,000</option>
                    <option value="300000">₦ 3,000</option>
                    <option value="500000">₦ 5,000</option>
                    <option value="1000000">₦ 10,000</option>
                    <option value="2000000">₦ 20,000</option>
                    <option value="3000000">₦ 30,000</option>
                    <option value="4000000">₦ 40,000</option>
                    <option value="5000000">₦ 50,000</option>
                    <option value="others">Others</option>
                </select>
              </fieldset>
              @if ($errors->has('amount'))
                <span class="help-block">
                    <strong>{{ $errors->first('amount') }}</strong>
                </span>
              @endif
              </div>
            <input type="hidden" name="purpose" value="contribution-fee" >
            @endif
            
          <div class="form-group">
            <button class="btn btn-success" type="submit" value="Pay Now!">
            <i class="fa fa-plus-circle fa-lg"></i> Record
            </button>
          </div>
        </div>
        </div>
</form>
@endsection