@extends('dashboard.master')
@section('title', 'Index page')
@section('content')
<div class="row" >
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>

<form style="margin-top:50px;" method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
        <div class="row" style="margin-bottom:40px;">
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h3>
              @if ($purpose == "Registration Fee")
              <div class="input-container">
                <label for="name" class="rl-label required">{{ $purpose }}</label>
                <input type="text" id="reg" name="reg" value="₦ 2,500" required="required" disabled>
              </div>
              @else
              <div class="input-container">
                <fieldset>
                <select name="amount" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                    <option style="margin-top: 1000px;" value="" >Contribution</option>
                    <option value="100000">₦ 1,000</option>
                    <option value="300000">₦ 3,000</option>
                    <option value="500000">₦ 5,000</option>
                    <option value="1000000">₦ 10,000</option>
                    <option value="2000000">₦ 20,000</option>
                    <option value="3000000">₦ 30,000</option>
                    <option value="4000000">₦ 40,000</option>
                    <option value="5000000">₦ 50,000</option>
                    <option value="others">Others</option>
                </select>
              </fieldset>
              </div>
              @endif
            </h3>
            <input type="hidden" name="email" value="{{ Auth::user()->member->email }}"> {{-- required --}}
            @if ($purpose == "Registration Fee")
              <input type="hidden" name="amount" value="250000"> {{-- required in kobo --}}
              <input type="hidden" name="metadata" value="{{ json_encode($array = ['purpose' => 'registration-fee', 'user_id' => Auth::user()->id, 'member_id' => Auth::user()->member->id]) }}" >
            @else
              <input type="hidden" name="metadata" value="{{ json_encode($array = ['purpose' => 'contribution-fee', 'user_id' => Auth::user()->id, 'member_id' => Auth::user()->member->id]) }}" >
            @endif
            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}

            <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}
            <!-- <input type="hidden" name="metadata" value="{{ json_encode($array = ['user_id' => Auth::user()->id]) }}" >
            <input type="hidden" name="metadata" value="{{ json_encode($array = ['member_id' => Auth::user()->member->id]) }}" > -->

            <p>
              <button class="btn btn-success" type="submit" value="Pay Now!">
              <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
              </button>
            </p>
          </div>
        </div>
</form>

<div class="auth-or">
    <hr class="hr-or">
    <span class="span-or">OR</span>
</div>


<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="padding-left: 30px;">
    <h4 class="text-uppercase"><strong>You can also make payment through transfer or deposit. Make use of the account Details below:</strong></h4>
    <h5><b>Account No  : <i>0037526466</i></b></h5>
    <h5><b>Account Name : <i>Raimi Ademola</i></b></h5>
    <h5><b>Bank        : <i>GT Bank</i></b></h5>
    <h6><span>***During payment, please use your email and phone number as reference!</span></h6>
    <h1 class="display-1" style="padding-left: 45px;"></h1>
</div>
