@extends('dashboard.master')
@section('title', 'change password')
@section('content')
<div class="row">
    @include('dashboard.partials.top-nav-bar')
</div>
<div class="row">
    @include('dashboard.partials.side-nav-bar')
</div>
<div style="margin-top:50px;" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" >
    <!-- <div class="row"> -->
        <!-- <div class="col-md-6 col-md-offset-3 card" style="margin-top: 2%;"> -->
            <h3>Change Password</h3>
            <hr>
            <form class="form" role="form" method="POST" action="{{ route('post-changepassword') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group{{ $errors->has('oldPassword') ? ' has-error' : '' }}">
                    <label for="oldPassword">Old Password</label>
                    <input type="password" class="form-control" name="oldPassword" value="">
                    @if ($errors->has('oldPassword'))
                    <span class="help-block">
                        <strong>{{ $errors->first('oldPassword') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">New Password</label>
                    <input type="password" class="form-control" name="password" value="">
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password">Confirm New Password</label>
                    <input type="password" class="form-control" name="password_confirmation" value="">
                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" style="background-color: #0f632e ! important; border: none;">
                    <i class="fa fa-btn fa-user"></i>Submit
                    </button>
                </div>
                
            </form>
            
        <!-- </div> -->
    <!-- </div> -->
</div>
@endsection
