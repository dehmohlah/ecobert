<table class="yiv7788605542wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;background-color:#f5f8fa;margin:0;padding:0;width:100%;">
        <tbody><tr>
            <td align="center" style="font-family:Avenir, Helvetica, sans-serif;">
                <table class="yiv7788605542content" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;margin:0;padding:0;width:100%;">
                    <tbody><tr>
    <td class="yiv7788605542header" style="font-family:Avenir, Helvetica, sans-serif;padding:25px 0;text-align:center;">
        <a rel="nofollow" target="_blank" href="{{ route('index') }}" style="font-family:Avenir, Helvetica, sans-serif;color:#bbbfc3;font-size:19px;font-weight:bold;text-decoration:none;text-shadow:0 1px 0 white;">
            Ecobert
        </a>
    </td>
</tr>

                    
                    <tr>
                        <td class="yiv7788605542body" width="100%" style="font-family:Avenir, Helvetica, sans-serif;background-color:#FFFFFF;border-bottom:1px solid #EDEFF2;border-top:1px solid #EDEFF2;margin:0;padding:0;width:100%;">
                            <table class="yiv7788605542inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;background-color:#FFFFFF;margin:0 auto;padding:0;width:570px;">
                                
                                <tbody><tr>
                                    <td class="yiv7788605542content-cell" style="font-family:Avenir, Helvetica, sans-serif;padding:35px;">
                                        <h1 style="font-family:Avenir, Helvetica, sans-serif;color:#2F3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left;">Hello!</h1>
<p style="font-family:Avenir, Helvetica, sans-serif;color:#74787E;font-size:16px;line-height:1.5em;margin-top:0;text-align:left;">You are receiving this email because we received a password reset request for your account. Note that the link will expire in 24 hours.</p>
<table class="yiv7788605542action" align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;margin:30px auto;padding:0;text-align:center;width:100%;">
    <tbody><tr>
        <td align="center" style="font-family:Avenir, Helvetica, sans-serif;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;">
                <tbody><tr>
                    <td align="center" style="font-family:Avenir, Helvetica, sans-serif;">
                        <table border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;">
                            <tbody><tr>
                                <td style="font-family:Avenir, Helvetica, sans-serif;">
                                    <a rel="nofollow" target="_blank" href="{{ route('password.reset', $token) }}" class="yiv7788605542button yiv7788605542button-blue" style="font-family:Avenir, Helvetica, sans-serif;border-radius:3px;color:#FFF;display:inline-block;text-decoration:none;background-color:#3097D1;border-top:10px solid #3097D1;border-right:18px solid #3097D1;border-bottom:10px solid #3097D1;border-left:18px solid #3097D1;">Reset Password</a>
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
<p style="font-family:Avenir, Helvetica, sans-serif;color:#74787E;font-size:16px;line-height:1.5em;margin-top:0;text-align:left;">If you did not request a password reset, no further action is required.</p>
<p style="font-family:Avenir, Helvetica, sans-serif;color:#74787E;font-size:16px;line-height:1.5em;margin-top:0;text-align:left;">Regards,<br>Ecobert</p>
<table class="yiv7788605542subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;border-top:1px solid #EDEFF2;margin-top:25px;padding-top:25px;">
    <tbody><tr>
        <td style="font-family:Avenir, Helvetica, sans-serif;">
            <p style="font-family:Avenir, Helvetica, sans-serif;color:#74787E;line-height:1.5em;margin-top:0;text-align:left;font-size:12px;">If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below
into your web browser: <a rel="nofollow" target="_blank" href="{{ route('password.reset', $token) }}" style="font-family:Avenir, Helvetica, sans-serif;color:#3869D4;"></a><a rel="nofollow" target="_blank" href="{{ route('password.reset', $token) }}" style="font-family:Avenir, Helvetica, sans-serif;color:#3869D4;">{{ route('password.reset', $token) }}</a></p>
        </td>
    </tr>
</tbody></table>

                                        
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>

                    <tr>
    <td style="font-family:Avenir, Helvetica, sans-serif;">
        <table class="yiv7788605542footer" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir, Helvetica, sans-serif;margin:0 auto;padding:0;text-align:center;width:570px;">
            <tbody><tr>
                <td class="yiv7788605542content-cell" align="center" style="font-family:Avenir, Helvetica, sans-serif;padding:35px;">
                    <p style="font-family:Avenir, Helvetica, sans-serif;line-height:1.5em;margin-top:0;color:#AEAEAE;font-size:12px;text-align:center;">© 2018 Ecobert. All rights reserved.</p>
                </td>
            </tr>
        </tbody></table>
    </td>
</tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>