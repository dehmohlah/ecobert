        <div class="shell">
          <hr>
        </div>
        <footer class="page-foot section section-35 bg-ebony-clay context-dark">
          <div class="shell text-center">
            <div class="range range-sm-reverse range-sm-justify range-sm-middle">
              <div class="cell-sm-6 text-sm-right">
                <div class="group-sm group-middle">
                  <p class="text-italic text-white">Follow Us:</p>
                  <ul class="list-inline list-inline-reset">
                    <li><a href="https://www.facebook.com/ecobertycooperativesociety" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-facebook"></a></li>
                    <li><a href="https://twitter.com/ecoberty" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-instagram"></a></li>

                  </ul>
                </div>
              </div>
              <div class="cell-sm-6 offset-top-15 offset-sm-top-0 text-sm-left">
                <p class="rights text-white"><span id="copyright-year"></span><span>&nbsp;&#169;&nbsp;</span><span>Ecoberty.&nbsp;</span><a href="/terms-condition" class="link-white-v2">Terms and Condtions</a>
                </p>
              </div>
            </div>
          </div>
        </footer>
      </div>

    {{-- </body> --}}
    {{-- <div id="form-output-global" class="snackbars"></div> --}}
    {{-- <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div> --}}

    <script src="//code.tidio.co/2wn6whfo37oksjvdmam4wnndwjsxsrv8.js"></script>

    <script src="{!! asset('sweetalert/sweetalert.min.js') !!}"></script>
        @include('sweet::alert')
    <script src="{!! asset('js/core.min.js') !!}"></script>
    <script src="{!! asset('js/script.js') !!}"></script>
    <script src="{!! load_asset('dashboard-styles/js/bootstrap.min.js') !!}"></script>
