<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <meta name="google-site-verification" content="FXiJOOyAsYSOWQ552UZ25_JPpPCLzoTWxWs-L6v-W0U" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="{!! load_asset('images/favicon.ico') !!}" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Arvo:400,700%7COpen+Sans:300,300italic,400,400italic,700italic,800%7CUbuntu:500">
    <link rel="stylesheet" href="{!! load_asset('css/style.css') !!}">
    <link rel="stylesheet" href="{!! load_asset('sweetalert/sweetalert.css') !!}">
    <link rel="stylesheet" href="{!! load_asset('font-awesome/css/font-awesome.min.css') !!}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="{!! load_asset('dashboard-styles/css/bootstrap.min.css') !!}" rel='stylesheet' type='text/css' media="all" />
        <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="httpqwertywindows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
  </head>
  <body style="">
    <div class="page">
      <header class="page-head">
        <div class="rd-navbar-wrap">
          <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="53px" data-lg-stick-up-offset="53px" data-md-stick-up="true" data-lg-stick-up="true" class="rd-navbar rd-navbar-corporate-light">
            <div class="bg-ebony-clay context-dark">
              <div class="rd-navbar-inner">
              </div>
            </div>
            <div class="rd-navbar-inner">
              <div class="rd-navbar-group">
                <div class="rd-navbar-panel">
                  <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button><a href="/" class="rd-navbar-brand brand"><img style="    width: 190px;
    height: 100px;
    margin-top: -50px;" class="logo-customize" src="{!! load_asset('images/ecoberty_logo.jpg') !!}" alt="" width="200" height="100"/></a>
                </div>
                <div class="rd-navbar-group-asside">
                  <div class="rd-navbar-nav-wrap">
                    <div class="rd-navbar-nav-inner">
                      <ul class="rd-navbar-nav">
                        <li class="active"><a href="/">Home</a>
                        </li>
                        <li><a href="/about">About us</a>
                        </li>
                        <li><a href="/services">Services</a>
                        </li>
                        <li><a href="/contact">Contact us</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!-- <div class="rd-navbar-search">
                    <form action="search-results.html" method="GET" data-search-live="rd-search-results-live" data-search-live-count="6" class="rd-search">
                      <div class="rd-search-inner">
                        <div class="form-group">
                          <label for="rd-search-form-input" class="form-label">Search...</label>
                          <input id="rd-search-form-input" type="text" name="s" autocomplete="off" class="form-control">
                        </div>
                        <button type="submit" class="rd-search-submit"></button>
                      </div>
                      <div id="rd-search-results-live" class="rd-search-results-live"></div>
                    </form>
                    <button data-rd-navbar-toggle=".rd-navbar-search, .rd-navbar-search-wrap" class="rd-navbar-search-toggle"><i class="fa fa-search" aria-hidden="true"></i></button>
                  </div> -->
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>