@include('layouts.header')

      <section style="background-image: url({!! load_asset('images/bg-image-1.jpg') !!});" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
  <div class="shell">
    <div class="page-title">
      <h2>Corporative Membership Forms</h2>
    </div>
  </div>
</section>
      <section class="section-35 section-sm-75 section-lg-100 bg-whisperapprox">
        <div class="shell">
          <div class="range">

            <div style="margin: 0 auto;" class="cell-md-9 cell-lg-6">
              <h3 style="margin-top: -70px;">Membership Form</h3>
              <form method="POST" action="{{ route('post-membership-form') }}" enctype="multipart/form-data">
                <div class="range">
              <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">

                    <legend>Please upload a recent Passport</legend>
                    <!-- <form id="uploadImage" class="form" method="POST" action="" enctype="multipart/form-data"> -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                        <div class="form-group">
                            <img src="http://www.gravatar.com/avatar?d=mm&s=500" title="avatar" alt="avatar" height="100" width="100" style="border-radius:25px;">
                        </div>
                        <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                            <input id="avatar" type="file" name="avatar" value="{{ old('avatar') }}" accept="image/*" data-constraints="@Required" class="form-control">
                        </div>
                        @if ($errors->has('avatar'))
                        <span style="color: red;" class="help-block">
                            <strong>{{ $errors->first('avatar') }}</strong>
                        </span>
                        @endif
                        <!-- <div class="form-group">
                            <button class="btn btn-primary" id="btnUpload" type="submit">
                            <i class="fa fa-plus"></i> Upload
                            </button>
                        </div>
                    </form> -->
                </div>

              
                
                  <div class="cell-sm-12 offset-top-30">
                    <h5 style="text-align: center;"> Personal Data</h5>
                  </div>



                  <div class="cell-sm-6 offset-top-30">
                    <label>Title</label>
                  <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <fieldset>
                        <select id="s2" name="title" value="{{ old('title') }}" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                            <option style="margin-top: 1000px;" value="" >Choose title</option>
                            <option value="mr">Mr</option>
                            <option value="mrs">Mrs</option>
                            <option value="miss">Miss</option>
                            <option value="others">Others</option>
                        </select>
                      </fieldset>
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="cell-sm-6 offset-top-30">
                      <label>Gender</label>
                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                      <fieldset>
                      <select id="s2" name="gender" value="{{ old('gender') }}" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                          <option style="margin-top: 1000px;" value="" >Choose gender</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </select>
                      </fieldset>
                        @if ($errors->has('gender'))
                            <span class="help-block">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                        @endif
                    </div>
                    </div>
                  <div class="cell-sm-6 offset-top-30 ">
                    <label>First Name</label>
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">

                      <input id="feedback-2-first-name" type="text" value="{{ old('first_name') }}" name="first_name" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-first-name" class="form-label">First Name</label>
                      @if ($errors->has('first_name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('first_name') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Last Name</label>
                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                      <input id="feedback-2-last-name" type="text" name="last_name" value="{{ old('last_name') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-last-name" class="form-label">Last Name</label>
                      @if ($errors->has('last_name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('last_name') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  
                  <div class="cell-sm-6 offset-top-30">
                    <label>Email</label>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      <input id="feedback-2-email" type="email" name="email" value="{{ old('email') }}" data-constraints="@Email @Required" class="form-control">
                      <label for="feedback-2-email" class="form-label">Email</label>
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="cell-sm-6 offset-top-30">
                    <label>Contact Address</label>
                    <div class="form-group{{ $errors->has('contact_address') ? ' has-error' : '' }}">
                      <input id="feedback-2-address" type="text" name="contact_address" value="{{ old('contact_address') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-address" class="form-label">Contact Address</label>
                      @if ($errors->has('contact_address'))
                          <span class="help-block">
                              <strong>{{ $errors->first('contact_address') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Phone Number 1</label>
                    <div class="form-group{{ $errors->has('phone_number_1') ? ' has-error' : '' }}">
                      <input id="feedback-2-phone_1" type="tel" name="phone_number_1" value="{{ old('phone_number_1') }}" data-constraints="@Numeric @Required" class="form-control">
                      <label for="feedback-2-phone_1" class="form-label">Phone Number 1</label>
                      @if ($errors->has('phone_number_1'))
                          <span class="help-block">
                              <strong>{{ $errors->first('phone_number_1') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Phone Number 2</label>
                    <div class="form-group{{ $errors->has('phone_number_2') ? ' has-error' : '' }}">
                      <input id="feedback-2-phone_2" type="tel" name="phone_number_2" value="{{ old('phone_number_2') }}" data-constraints="@Numeric" class="form-control">
                      <label for="feedback-2-phone_2" class="form-label">Phone Number 2</label>
                      @if ($errors->has('phone_number_2'))
                          <span class="help-block">
                              <strong>{{ $errors->first('phone_number_2') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Postal Address</label>
                    <div class="form-group{{ $errors->has('postal_address') ? ' has-error' : '' }}">
                      <input id="feedback-2-postal_address" type="text" name="postal_address" value="{{ old('postal_address') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-postal_address" class="form-label">Postal Address</label>
                      @if ($errors->has('postal_address'))
                          <span class="help-block">
                              <strong>{{ $errors->first('postal_address') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Date Of Birth</label>
                    <div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
                      <input id="feedback-2-dob" type="date" name="date_of_birth" value="{{ old('date_of_birth') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-dob" class="form-label">Date of Birth</label>
                      @if ($errors->has('date_of_birth'))
                          <span class="help-block">
                              <strong>{{ $errors->first('date_of_birth') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Place of Birth</label>
                    <div class="form-group{{ $errors->has('place_of_birth') ? ' has-error' : '' }}">
                      <input id="feedback-2-pob" type="text" name="place_of_birth" value="{{ old('place_of_birth') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-pob" class="form-label">Place of Birth</label>
                      @if ($errors->has('place_of_birth'))
                          <span class="help-block">
                              <strong>{{ $errors->first('place_of_birth') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Nationality</label>
                    <div class="form-group{{ $errors->has('nationality') ? ' has-error' : '' }}">
                      <input id="feedback-2-nationality" type="text" name="nationality" value="{{ old('nationality') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-nationality" class="form-label">Nationality</label>
                      @if ($errors->has('nationality'))
                          <span class="help-block">
                              <strong>{{ $errors->first('nationality') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>State of Origin</label>
                    <div class="form-group{{ $errors->has('state_of_origin') ? ' has-error' : '' }}">
                      <input id="feedback-2-origin" type="text" name="state_of_origin" value="{{ old('state_of_origin') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-origin" class="form-label">State Of Origin</label>
                      @if ($errors->has('state_of_origin'))
                          <span class="help-block">
                              <strong>{{ $errors->first('state_of_origin') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Home Town/Village of Origin</label>
                    <div class="form-group{{ $errors->has('home_town_village_of_origin') ? ' has-error' : '' }}">
                      <input id="feedback-2-village_of_origin" type="text" name="home_town_village_of_origin" value="{{ old('home_town_village_of_origin') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-village_of_origin" class="form-label">Home Town/Village of Origin</label>
                      @if ($errors->has('home_town_village_of_origin'))
                          <span class="help-block">
                              <strong>{{ $errors->first('home_town_village_of_origin') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Local Government Area</label>
                    <div class="form-group{{ $errors->has('local_government_area') ? ' has-error' : '' }}">
                      <input id="feedback-2-lga" type="text" name="local_government_area" value="{{ old('local_government_area') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-lga" class="form-label">Local Government Area</label>
                      @if ($errors->has('local_government_area'))
                          <span class="help-block">
                              <strong>{{ $errors->first('local_government_area') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-12 offset-top-30">
                    <h5 style="text-align: center;">Details on next of kin</h5>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                   <label>Name of Next of Kin</label>
                    <div class="form-group{{ $errors->has('name_of_next_kin') ? ' has-error' : '' }}">
                      <input id="feedback-2-kin" type="text" name="name_of_next_kin" value="{{ old('name_of_next_kin') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-kin" class="form-label">Name of Next of Kin</label>
                      @if ($errors->has('name_of_next_kin'))
                          <span class="help-block">
                              <strong>{{ $errors->first('name_of_next_kin') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="cell-sm-6 offset-top-30">
                    <label>Address of Next of Kin</label>
                    <div class="form-group{{ $errors->has('address_of_next_kin') ? ' has-error' : '' }}">
                      <input id="feedback-2-kin_address" type="text" name="address_of_next_kin" value="{{ old('address_of_next_kin') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-kin_address" class="form-label">Address of Next of Kin</label>
                      @if ($errors->has('address_of_next_kin'))
                          <span class="help-block">
                              <strong>{{ $errors->first('address_of_next_kin') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>

                  <div class="cell-sm-6 offset-top-30">
                    <label>Phone of Next of Kin</label>
                    <div class="form-group{{ $errors->has('phone_of_next_kin') ? ' has-error' : '' }}">
                      <input id="feedback-2-kin_phone" type="text" name="phone_of_next_kin" value="{{ old('phone_of_next_kin') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-kin_phone" class="form-label">Phone of Next of Kin</label>
                      @if ($errors->has('phone_of_next_kin'))
                          <span class="help-block">
                              <strong>{{ $errors->first('phone_of_next_kin') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  
                </div>
                    <div class="cell-sm-6 offset-top-30 offset-sm-top-50">
                      <button type="submit" name="saveContinueLater" class="btn btn-rect btn-ebony-clay-outline btn-block">Save And continue Later</button>
                    </div>
                    <div class="cell-sm-6 offset-top-30">
                      <button  type="submit" name="saveGoNextPage" class="btn btn-rect btn-ebony-clay-outline btn-block">Save and move to Next page</button>
                    </div>
                  </div>
              </form>
      </section>
      @include('layouts.footer')
  </body>
</html>
