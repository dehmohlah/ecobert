@include('layouts.header')

      <section style="background-image: url({!! load_asset('images/bg-image-1.jpg') !!});" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
  <div class="shell">
    <div class="page-title">
      <h2>Corporative Membership Forms</h2>
    </div>
  </div>
</section>
      <section class="section-35 section-sm-75 section-lg-100 bg-whisperapprox">
        <div class="shell">
          <div class="range">

            <div style="margin: 0 auto;" class="cell-md-9 cell-lg-6">
              <form method="POST" action="{{ route('post-members-form-next') }}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
                <div class="range">
                  <div style="margin-top: -70px;" class="cell-sm-12">
                    <h5 style="text-align: center;"> Details on current work place</h5>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Company name and Address</label>
                    <div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }}">
                      <input id="feedback-2-company_address" type="text" name="company_address" value="{{ old('company_address') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-company_address" class="form-label">Company name and Address</label>
                      @if ($errors->has('company_address'))
                          <span class="help-block">
                              <strong>{{ $errors->first('company_address') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Post</label>
                    <div class="form-group{{ $errors->has('post') ? ' has-error' : '' }}">
                      <input id="feedback-2-post" type="text" name="post" value="{{ old('post') }}" data-constraints="@Required" class="form-control">
                      <label for="feedback-2-post" class="form-label">Post</label>
                      @if ($errors->has('post'))
                          <span class="help-block">
                              <strong>{{ $errors->first('post') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                    <label>Date join Company</label>
                    <div class="form-group{{ $errors->has('date_join_company') ? ' has-error' : '' }}">
                      <input id="date-join-company" type="date" name="date_join_company" value="{{ old('date_join_company') }}" data-constraints="@Required" class="form-control">
                      <label for="date-join-company" class="form-label">Date join Company</label>
                      @if ($errors->has('date_join_company'))
                          <span class="help-block">
                              <strong>{{ $errors->first('date_join_company') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-xs-12 offset-top-30">
                    <label>Responsibilities</label>
                    <div class="form-group{{ $errors->has('responsibilities') ? ' has-error' : '' }}">
                      <textarea id="responsibilities" type="date" name="responsibilities" value="{{ old('responsibilities') }}" data-constraints="@Required" class="form-control"></textarea>
                      <label for="responsibilities" class="form-label">Responsibilities</label>
                      @if ($errors->has('responsibilities'))
                          <span class="help-block">
                              <strong>{{ $errors->first('responsibilities') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30">
                     <label>Monthly Contribution</label>
                  <div class="form-group{{ $errors->has('monthly_contribution') ? ' has-error' : '' }}">
                        <fieldset>
                        <select id="s2" name="monthly_contribution" data-minimum-results-for-search="Infinity" data-constraints="@Required" class="form-control select-filter">
                            <option style="margin-top: 1000px;" value="" >Monthly Contribution</option>
                            <option value="1000">1,000</option>
                            <option value="3000">3,000</option>
                            <option value="5000">5,000</option>
                            <option value="10000">10,000</option>
                            <option value="20000">20,000</option>
                            <option value="30000">30,000</option>
                            <option value="40000">40,000</option>
                            <option value="50000">50,000</option>
                            <option value="others">Others</option>
                        </select>
                      </fieldset>
                        @if ($errors->has('monthly_contribution'))
                            <span class="help-block">
                                <strong>{{ $errors->first('monthly_contribution') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="cell-sm-12 offset-top-30">
                      <h5 style="text-align: center;"> Details of introducer</h5>
                    </div>
                    <div class="cell-sm-6 offset-top-30">
                      <label>Introducer Full name</label>
                    <div class="form-group{{ $errors->has('introduced_by') ? ' has-error' : '' }}">
                      <input id="introduced_by" type="text" name="introduced_by" value="{{ old('introduced_by') }}" data-constraints="@Required" class="form-control">
                      <label for="introduced_by" class="form-label">Introducer Full name</label>
                      @if ($errors->has('introduced_by'))
                          <span class="help-block">
                              <strong>{{ $errors->first('introduced_by') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-xs-6 offset-top-30">
                    <label>Introducer Phone Number</label>
                    <div class="form-group{{ $errors->has('introducer_number') ? ' has-error' : '' }}">
                      <input id="introducer_number" type="phone" name="introducer_number" value="{{ old('introducer_number') }}" data-constraints="@Required @Numeric" class="form-control">
                      <label for="introducer_number" class="form-label">Introducer Phone Number</label>
                      @if ($errors->has('introducer_number'))
                          <span class="help-block">
                              <strong>{{ $errors->first('introducer_number') }}</strong>
                          </span>
                      @endif
                    </div>
                  </div>
                  <div class="cell-sm-12 offset-top-30">
                      <h5 style="text-align: center;">Declaration</h5>
                    </div>
                  <div class="cell-xs-12 offset-top-30 btn-primary btn-block">
                    <strong>I hereby apply as a
                      member of ECOBERTY MULTI-PURPOSE COOPERATIVE
                      SOCIETY LIMITED. If admitted, I undertake to accept and abide by the code of
                      conduct/Covenant/Rules & Bye-laws of the Cooperative and shall endeavour to advance
                      the course of the Organization. I certify that the information given on this form is true
                      and correct and enclose payment for my membership application.
                    </strong>    
                  </div>


                  <div class="button_part" style="width: 100%; float: left; text-align: center; margin: 50px 0 0 0">
            
                  <div id="signArea" >
                    <h5 style="text-align: center;">Put signature below,</h5>
                  <div class="form-group{{ $errors->has('signature') ? ' has-error' : '' }}" style="height:auto;">
                    <div class="typed"></div>
                    <canvas id="signature-pad" style="background-color: #fff;" class="signature-pad" width=400 height=200></canvas>
                  </div>
                  </div>
                  <button id="draw" class="part_btn" style="font-size: 18px; background-color: green; border: 1px solid green; color: #FFF; padding: 7px 30px 7px 30px; width: fit-content; margin-bottom: 20px; margin-top: 10px; float: center;">Draw</button>
                  <button id="erase" class="part_btn" style="font-size: 18px; background-color: green; border: 1px solid green; color: #FFF; padding: 7px 30px 7px 30px; width: fit-content; margin-bottom: 20px; margin-top: 10px; float: center;">Erase</button>
                  <button id="clear" class="part_btn" style="font-size: 18px; background-color: green; border: 1px solid green; color: #FFF; padding: 7px 30px 7px 30px; width: fit-content; margin-bottom: 20px; margin-top: 10px; float: center;">Clear</button>
                </div>
                  <div class="cell-sm-6 offset-top-30 offset-sm-top-50 center-block">
                    <button style="" type="submit" class="btn btn-rect btn-ebony-clay-outline btn-block">Save</button>
                  </div>
                  </div>
              </form>
      </section>


      @include('layouts.footer')
      <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>    
      <script>
        var canvas = document.querySelector("canvas");
        var signaturePad = new SignaturePad(canvas);
    
        signaturePad.penColor = "rgb(66, 133, 244)";


        $("#clear").click(function(event){
          event.preventDefault();
             signaturePad.clear();
           });

        $("#draw").click(function(event){
          event.preventDefault();
          var ctx = canvas.getContext('2d');
          console.log(ctx.globalCompositeOperation);
          ctx.globalCompositeOperation = 'source-over'; // default value
        });

        $("#erase").click(function(event){
          event.preventDefault();
          var ctx = canvas.getContext('2d');
          ctx.globalCompositeOperation = 'destination-out';
        });
        
        $('form').on('submit', function (e) {
          e.preventDefault();

          if (signaturePad.isEmpty()) {
            return alert("Please provide a signature first.");
          }

          swal({
            title: "Saving...",
            text: "Please wait",
            imageUrl: "{!! load_asset('images/ajax-loader.gif') !!}",
            showConfirmButton: false,
            allowOutsideClick: false
          });
        
          var data = signaturePad.toDataURL('image/png');
          var img_data = data.replace(/^data:image\/(png|jpg);base64,/, "");

          var userId = $('input[name=user_id]').val();
          var companyAddress = $('input[name=company_address]').val();
          var post = $('input[name=post]').val();
          var dateJoinCompany = $('input[name=date_join_company]').val();
          var responsibilities = $('textarea[name=responsibilities]').val();
          var monthlyContribution = $('select[name=monthly_contribution]').val();
          var introducedBy = $('input[name=introduced_by]').val();
          var introducerNumber = $('input[name=introducer_number]').val();
          var url = "{{ route('post-members-form-next') }}";

          var formData = {
            'user_id' : userId,
            'company_address' : companyAddress,
            'post'             : post,
            'date_join_company'    : dateJoinCompany,
            'responsibilities' : responsibilities,
            'monthly_contribution' : monthlyContribution,
            'introduced_by'    : introducedBy,
            'introducer_number'    : introducerNumber,
            'img_data' : img_data
          };
          $.ajax({
            type: 'post',
            url: url,
            data: formData,
            dataType: 'json',
            success: function(res){
              swal({
                title: "Finished!",
                showConfirmButton: false,
                timer: 3000
              });
              console.log(res.message)
               if (res.message == "succesfully updated") {
                swal({
                  title: "Success",
                  text: "You have succesfully completed your membership registration; We will get back to you after reviewing",
                  type: 'success',
                });
                window.location = "{{ route('dashboard') }}";
              };
              
            },
            error: function(data){
              var errors = data.responseJSON;
              console.log(errors.errors.company_address);

              var target = errors.errors;
              for (var k in target){
                if (target.hasOwnProperty(k)) {
                    swal({
                      title: "Error",
                      text: target[k],
                      type: 'error',
                    });
                }
            }
            }
          });
        });
      </script> 
  </body>
</html>

                