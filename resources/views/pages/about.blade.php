@include('layouts.header')


      <section style="background-image: url(images/bg-image-1.jpg);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
        <div class="shell">
          <div class="page-title">
            <h2>About Us</h2>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <h3>Who we are?</h3>
                <p>Ecoberty Cooperative is a society established to promote savings culture and support the
                  mutual social and economic development of our members by organizing a pool of funds
                  to serve as reliable source of credit and investment opportunities, which members can
                  conveniently and affordably access for personal and business development.</p>
                <p>
                  Ecoberty Cooperative Multipurpose Society Limited (Ecoberty Cooperative) is registered
                  under the provisions of the Cooperative Societies Law and Regulations of Lagos State
                  Government on 19 October 2017 with Registration Number LSCS 15768. The
                  cooperative is set up to promote saving culture, mobilize funds for those who have
                  genuine entrepreneurial vision, provide financial education and position our members
                  to access financial and technical assistance from the government and other sources. The
                  society is set to challenge the existing awkward lending conditions in Nigeria and
                  popularize cooperative approach to finance in Africa.
                </p>
                <p>
                  Ecoberty is set to create an online cooperative platform where members can join and
                  access the cooperative’s products and services in the most transparent manner.
                </p>
                <p>Our members are people with whom we share common bond; those who are young,
                aspiring, striving or thriving entrepreneurs, students, civil servants, artisans, farmers
                and traders, to meet their personal, personal, economic, business, social and academic
                aspirations through a jointly owned society - Ecoberty Cooperative Multipurpose
                Society Limited.
                </p>
                <h4>Our vision</h4>
                <p>Our vision is to be the most supportive and responsive cooperative society in Africa.
                </p>
                <h4>Our mission</h4>
                <p>Our mission is to be the agent of economic and social liberation for our members and we
                will achieve this by:
                </p>
                <ul class="list-item">
                  <li>providing innovative financial products and services targeted at meeting our members’ needs</li>
                  <li>effectively managing the resources of the Society to maximize our members’ wealth</li>
                  <li>maintaining a structure that uphold cooperatives’ principles of growth and expansion</li>
                  <li>organizing financial education programs to provide financial literacy for our members</li>
                  <li>proactively sourcing and identifying technical assistance and funding opportunities for our members’ entrepreneurial efforts</li>
                  <li>providing financial planning services to individual members and their business enterprises</li>
                </ul>
                <h4>Our Value proposition</h4>
                <p>
                    Our value propositions are hinged on the following Six (6) pillars:
                  </p><br>
                  <h5>Voluntary and open membership</h5>
                  <p>
                    Membership is voluntary and it is open to anyone above sixteen (16) years of sound
                    mind and character who support our principles and values. We are a not-for-profit
                    registered co-operative that is owned by its members. All profits generated from our
                    products and service offerings go back into the society for improvements to services,
                    financial reserves, higher returns on savings and lower interest on loans. As a member
                    of Ecoberty Cooperative, you have access to a full array of products and services
                    designed to help you save money, save time, avoid stress and satisfy your financial goals.
                    Our members are at the heart of everything we do at Ecoberty Cooperative. We will go
                    extra-mile to ensure that our members are happy and satisfied through regular and
                    timely updates and news, transparent reporting, free and fair voting and prompt
                    complaints and issues resolution. Our social media channels are active to receive
                    members’ request for timely processing and we will provide feedback within 24 hours.
                    We will continually seek opportunities to maximize benefits accruing to all our
                    members. Members can assess all the cooperative products and services and view
                    transaction statement online via the cooperative’s website.
                  </p>
                  <h5>Collaborative effort</h5>
                  <p>
                    There is awesome power and amazing possibilities in collaborative efforts. We believe
                    people can achieve much more together than individually, entrepreneurs and SME
                    owners can achieve their dreams with or without the exorbitant traditional bank loans.
                    We believe that young people deserve to invest their skills and energy in their own
                    future rather than scrambling for scarce opportunities with big multinational
                    corporations. Young people can create chains of employment opportunities; rather than
                    look for one, they can come together, save, share, lend and invest together to form a
                    formidable frontage for their individual and collective success.
                  </p>

                  <h5>Education training and information</h5>
                  <p>
                    We believe that a mind that knows is a mind that’s free, that is why we invest in
                    providing essential financial education for our members across all ages, educational and
                    social status. We have designed targeted financial education package to enrich the
                    minds of our members on budgeting and financial goal setting, saving, credit, banking
                    and other financial products and services, risk management and retirement planning.
                  </p>

                  <h5>Savings culture</h5>
                  <p>
                    We believe that saving is at the heart of financial liberty. We believe that the difference
                    between where you are and where you want to be financially is not based on how much
                    money you make, rather, it is based on how much money is set aside and saved for
                    future investment. We believe that financial liberty cannot arise without intentional and
                    die-hard commitment to long-term focused saving culture. Hence, to engender saving
                    habit, we shall continually be creating opportunities for people to save conveniently and
                    consistently for a very long time and this will help to build strong financial and
                    economic reserve that they can deploy when the need arise. To this end, we have
                    designed daily, monthly, weekly, quarterly and annual saving products and we will work
                    with you to implement effective saving plan that will help you achieve your financial
                    goals.
                  </p>

                  <h5>Financial inclusion</h5>
                  <p>
                    We believe that the financial market has great potential for everyone and we strive to
                    bring as many people as possible to participate in the financial market place, having
                    equipped with the right knowledge, while also providing support for each individual to
                    ensure that he/she maximize the benefits while reducing the risk. To this end, we are
                    providing micro-pension and micro-insurance services to our members in the informal
                    sector and rural areas.
                  </p>

                  <h5>Integrity</h5>
                  <p>
                    We act ethically in all matters without exception. We understand the true definition of integrity and we intentionally aim to walk in integrity and enroll our members to do the same.
                  </p>
                  </div>
      </section>
      @include('layouts.footer')
  </body>
</html>
