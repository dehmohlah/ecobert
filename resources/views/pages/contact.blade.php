@include('layouts.header')
      <section style="background-image: url(images/bg-image-4.jpg);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
        <div class="shell">
          <div class="page-title">
            <h2>Contacts</h2>
          </div>
        </div>
      </section>

      <section>
        <div class="shell">
          <div class="range range-md-justify">
            <div class="cell-md-5 cell-lg-4">
              <div class="inset-md-right-15 inset-lg-right-0">
                <div class="range">
                  <div class="cell-sm-10 cell-md-12">
                    <h3>How to Find Us</h3>
                    <p class="offset-sm-top-40 text-secondary">
                      If you have any questions, just fill in the contact form, and we will answer you shortly.
                      
                    </p>
                  </div>
                  
                  <div class="cell-sm-6 cell-md-12 offset-top-30 offset-sm-top-45 offset-md-top-30">
                    <h4>Support Centre</h4>
                    <address class="contact-info">
                      <h5>Telephones</h5>
                      <dl class="list-terms-inline">
                        <dt>Tosin</dt>
                        <dd><a href="callto:#" class="link-secondary">+234-706-235-4492.</a></dd>
                      </dl>
                      <dl class="list-terms-inline">
                        <dt>Niyi</dt>
                        <dd><a href="callto:#" class="link-secondary">+234-806-368-0464.</a></dd>
                      </dl>
                      <dl class="list-terms-inline">
                        <dt>Tayo</dt>
                        <dd><a href="callto:#" class="link-secondary">+234-706-511-6701.</a></dd>
                      </dl>
                      <br>
                      <dl class="list-terms-inline">
                        <dt>E-mail</dt>
                        <dd><a href="mailto:#" class="link-primary">membership@ecobertycoop.com</a></dd>
                      </dl>
                    </address>
                  </div>
                </div>
              </div>
            </div>
            <div class="cell-md-7 cell-lg-6 offset-top-50 offset-md-top-0">
              <h3>Get in Touch</h3>
              <form method="post" action="/rd-mailform" class="rd-mailform form-modern offset-top-30">
                {{ csrf_field() }}
                <div class="range">
                  <div class="cell-sm-6">
                    <div class="form-group">
                      <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control">
                      <label for="contact-name" class="form-label">Name</label>
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-30 offset-sm-top-0">
                    <div class="form-group">
                      <input id="contact-email" type="email" name="email" data-constraints="@Email @Required" class="form-control">
                      <label for="contact-email" class="form-label">Email</label>
                    </div>
                  </div>
                  <div class="cell-xs-12 offset-top-30">
                    <div class="form-group">
                      <div class="textarea-lined-wrap">
                        <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control"></textarea>
                        <label for="contact-message" class="form-label">Message</label>
                      </div>
                    </div>
                  </div>
                  <div class="cell-xs-8 offset-top-30 offset-xs-top-30 offset-sm-top-50">
                    <button type="submit" class="btn btn-rect btn-primary btn-block">Send</button>
                  </div>
                  <div class="cell-xs-4 offset-top-22 offset-xs-top-30 offset-sm-top-50">
                    <button type="reset" class="btn btn-rect btn-silver-outline btn-block">Reset</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      @include('layouts.footer')
  </body>
</html>
