@include('layouts.header')


      <section style="background-image: url(images/bg-image-1.jpg);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
        <div class="shell">
          <div class="page-title">
            <h2>Services</h2>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <h3>Product and services</h3>
          <ul class="list-item">
            <li>Regular Savings Account (Thrift)</li>
            <li>Personal and Business Loan (Credit)</li>
            <li>Equipment &amp; Leasing</li>
            <li>Ecoberty stores</li>
            <li>Education, Training &amp; Information</li>
            <li>Microinsurance</li>
            <li>Micropension</li>
          </ul><br>
        <h4>Regular savings account (thrift)</h4>
        <p>
          Depending on your source of income, we have daily, monthly, weekly, quarterly and
          annual saving products and we can work with you to implement effective saving plan
          that will help you achieve your financial goals.
        </p>
        <p>All savings/contribution should be paid to the Cooperative’s Bank Account with details
            below:</p>
        <h5>BANK NAME: GUARANTY TRUST BANK PLC (GTB)</h5>
        <h5>ACCOUNT NAME:  ECOBERTY (KOSOFE) C.M.S. LTD</h5>
        <h5>ACCOUNT NUMBER:  0269433336</h5>
        <p>Please indicate your name and membership number in the remark section of the payment transfer instruction.</p>
        <h4>Personal and Business Loan (Credit)</h4>
        <p>Only our members can be primary beneficiary of our loan/credit schemes.
            Members can take loan amount up to two times (or more) the value of their total savings subject to terms and conditions at a fair and fixed rate of interest. 
            To access any loan, member must have been registered with the Cooperative and making regular contributions for at least six months. The minimum loan application requirements must have been fully met. </p>
          <p>Loans are insured at a specified cost to the eligible member. 
              Repayments are calculated on the reducing balance of the loan. This means smaller interest repayments as you repay your loan. 
              Repayment terms will be agreed to suit your particular circumstances. You can repay the loan earlier or make larger repayments than agreed with no penalty. However, in event of default, there shall be penalty.<br>
              <a href="loan-applications">Click here to apply for loan</a></p>
            <h4>Equipment and leasing</h4>
            <p>We finance the acquisition of cars, office and business equipment for our members at affordable rates.  Similar to personal and business loans, only our members can be the direct beneficiary of the asset to be financed.  To access this product, member must have been registered with the Cooperative and made regular contributions for at least six months. The minimum application requirements must have been fully met.<br>
              <a href="loan-applications">Click here to apply for equipment finance</a></p>
            <h4>Education, Training & Information</h4>
            <p>
              We provide targeted financial education service to enrich the minds of our members and non-members on various topics such as budgeting and financial planning, saving, credit, banking and other financial products and services, risk management, retirement planning amongst others. We also provide information on activities of government and other agencies that could be of benefit to our members through our quarterly information bulletin.<br>
              <a href="#">Click here to read our quarterly information bulletin</a><br><a href="#">Click here to see our training calendar and apply for training</a></p>
            <h5>Ecoberty Stores</h5>
            <p>
              At Ecoberty Stores, we buy goods and consumables in bulk and sell in small packages to our members at very affordable, below-the-market prices.Non-members can also purchase from our store. 
            <br>
              <a href="#">Click here to visit our store</a></p>
              <h5>Micropension</h5>
              <p>
                In Nigeria, coverage of pension in the informal sector is nonexistent, however, the National Pension Commission is currently in the process of setting up an appropriate micro-pension structure. At Ecoberty, we encourage sole proprietors, artisans, traders and other workers in the informal sector to come under the contributory pension scheme and voluntarily accumulate savings for a long period of time to prepare for the reduction in earning capacity that accompanies old age primarily as a result of decline in health.  Under the micro-pension scheme every contribution made by members shall be split into two comprising 25% for contingent withdrawal and 75% for retirement benefits
              </p>
              <P>Ecoberty assists individual members to register with reputable micropension fund administrators and serve as fund collectors and aggregators for onward remittance to pension fund administrators.  We also assist to process payment of benefits and other customer service supports to our members.<br><a href="#">Click here to apply for Ecoberty Micro-pension Service</a><br><a href="#">To pay your pension contribution, click here</a></P>
              <h5>Microinsurance</h5>
              <p>
                At Ecoberty, we educate our members on risk management and encourage them to take insurance cover to mitigate risk. Ecoberty assists members to select insurance products from reputable insurance companies. We also serve as agent for onward remittance to premium and filing of claims when the need arise.<br><a href="contact">Click here to contact our Administrator</a>
              </p>
              <h5>Advisory and consultancy</h5>
              <p>
                Timely advice and real-time technical assistance have occasionally been proven to be very valuable than money.  At Ecoberty, we believe that quality financial advisory services should be made available and affordable to everyone that needs it.  We know that the cost of engaging well-qualified advisors and consultants can be extremely high and unaffordable for small business operators and young people.  Hence, we have designed plans to empower our members with low cost financial advisory and consultancy services, ranging from financial planning, budgeting, company/business registration, fund raising, succession planning, dispute resolution, debt recovery, forex sourcing and management, raw material/supplier sourcing, tax planning, obtaining tax clearance, business tourism, legal and business search, training and human resources, amongst others.<br><a href="contact">Click here to contact our Administrator</a>
              </p>
      </section>
      @include('layouts.footer')
  </body>
</html>
