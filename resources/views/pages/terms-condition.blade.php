@include('layouts.header')

      <section style="background-image: url(images/bg-privacy.jpg);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
        <div class="shell">
          <div class="page-title">
            <h2>Terms and Conditions</h2>
          </div>
        </div>
      </section>

      <section class="section-60 section-sm-90 section-lg-bottom-120">
        <div class="shell">
          <h3>Service Condition</h3>
          <div class="range range-sm-center offset-top-40 offset-sm-top-60">
            <div class="cell-lg-10">
              <div id="accordionOne" role="tablist" aria-multiselectable="true" class="panel-group panel-group-custom panel-group-light">
                <div class="panel panel-custom panel-light">
                  <div id="accordionOneHeading1" role="tab" class="panel-heading">
                    <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordionOne" href="#accordionOneCollapse1" aria-controls="accordionOneCollapse1" aria-expanded="true">General information
                        <div class="panel-arrow"></div></a>
                    </div>
                  </div>
                  <div id="accordionOneCollapse1" role="tabpanel" aria-labelledby="accordionOneHeading1" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <ul class="text-gray-05">
                        <li >
                           Only members would be primary beneficiary of any of our services.
                        </li>
                        <li>Regular savings/contributions can be made by members directly to the dedicated
                              Cooperative accounts
                        </li>
                        <li>Members can take loan amount up to two times (or more) the value of their total
                              savings subject to terms and conditions at a fair and fixed rate of interest.
                        </li>
                        <li>The repayment is as agreed with member.
                        </li>
                        <li>To access any loan, member must have been registered with the Cooperative and
                              making regular contributions for at least six months. The minimum loan application
                              requirements must have been fully met.
                        </li>
                        <li>Loans are insured at a specified cost to the eligible member.
                        </li>
                        <li>No hidden fees or transaction charges.
                        </li>
                        <li>Repayments are calculated on the reducing balance of the loan. This means smaller
                              interest repayments as you repay your loan.
                        </li>
                        <li>Repayment terms to suit your particular circumstances. You can repay the loan earlier
                              or make larger repayments than agreed with no penalty. However, in event of default, there shall be penalty.
                        </li>
                      </ul>
                      
                    </div>
                  </div>
                </div>
                <div class="panel panel-custom panel-light">
                  <div id="accordionOneHeading2" role="tab" class="panel-heading">
                    <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordionOne" href="#accordionOneCollapse2" aria-controls="accordionOneCollapse2" class="collapsed">Membership
                        <div class="panel-arrow"></div></a>
                    </div>
                  </div>
                  <div id="accordionOneCollapse2" role="tabpanel" aria-labelledby="accordionOneHeading2" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p class="text-gray-05">Membership is voluntary and it is open to anyone above eighteen (18) years of sound mind and character who support our principles and values. We are a not-for-profit registered co-operative that is owned by its members. That means profits generated go back into the society for improvements to services, financial reserves, higher savings rates and lower loan rates. As a member of Ecoberty Cooperative, you have access to a full array of products and services designed to help you save time money and satisfy your financial goals.</p>
                    </div>
                  </div>
                </div>

                <div class="panel panel-custom panel-light">
                  <div id="accordionOneHeading3" role="tab" class="panel-heading">
                    <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordionOne" href="#accordionOneCollapse3" aria-controls="accordionOneCollapse3" class="collapsed">How to become a member
                        <div class="panel-arrow"></div></a>
                    </div>
                  </div>
                  <div id="accordionOneCollapse3" role="tabpanel" aria-labelledby="accordionOneHeading3" class="panel-collapse collapse">
                    <div class="panel-body">
                      <ul class="text-gray-05">
                        <li>Membership entrance fee is N2,500.</li>
                        <li>You can become a member by completing the membership application form in hardcopy or online at <a href="/apply">www.ecobertycoop.com.ng/apply.</a></li>
                        <li>You can also email membership@ecobertycoop.com, Call or Whatsapp our membership
                          hotline 0706-235-4492.
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
          </div>
        </div>
      </section>
      @include('layouts.footer')
  </body>
</html>