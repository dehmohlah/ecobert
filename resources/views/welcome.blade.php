@include('layouts.header')
      <section>
        <div data-loop="false" data-autoplay="false" data-slide-delay="20s" data-simulate-touch="true" class="swiper-container swiper-slider swiper-variant-1 bg-gray-base carousel-inner">
          <div class="swiper-wrapper text-center">
            <div data-slide-bg="{!! load_asset('images/home-slider-1-slide-1.jpg') !!}" class="swiper-slide item active">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-sm-center">
                    <div class="cell-sm-11 cell-md-10 cell-lg-9">
                      <div data-caption-animate="fadeInUp" data-caption-delay="20s" class="shilder-header-with-divider veil reveal-md-block">We believe people can achieve much more together than individually</div>
                      <h2 data-caption-animate="fadeInUp" data-caption-delay="100s" class="slider-header">Welcome to Ecoberty<br>Multipurpose Cooperative Society</h2>
                      @if (! Auth::check())
                      <div class="offset-top-30 offset-sm-top-35">
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/register" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-user-plus" aria-hidden="true"></i>Register
                        </a>
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/login" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>Login
                        </a>
                      </div>
                      @else
                      <div class="offset-top-30 offset-sm-top-35">
                        @can ( 'registered-user', Auth::user()->role_id )
                        @can ( 'incomplete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form-next') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Resume Membership Form
                        </a>
                        @endcan
                        @can ( 'complete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @can ( 'exco-user', (Auth::user()->role_id) )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @endcan
                        @can ( 'become-members', Auth::user()->role_id )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Become a member
                        </a>
                        @endcan
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/logout" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-sign-out" aria-hidden="true"></i>Logout
                        </a>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div style="background: rgba(0, 0, 0, 0.7);" data-slide-bg="{!! load_asset('images/slider-1.jpg') !!}" class="swiper-slide item">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-sm-center">
                    <div class="cell-sm-11 cell-md-10 cell-lg-9">
                      <div data-caption-animate="fadeInUp" data-caption-delay="30s" class="shilder-header-with-divider veil reveal-md-block">Investment in the cooperative is an investment in your future</div>
                      <h2 data-caption-animate="fadeInUp" data-caption-delay="100s" class="slider-header">Welcome to Ecoberty<br>Multipurpose Cooperative Society</h2>
                      @if (! Auth::check())
                      <div class="offset-top-30 offset-sm-top-35">
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/register" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-user-plus" aria-hidden="true"></i>Register
                        </a>
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/login" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>Login
                        </a>
                      </div>
                      @else
                      <div class="offset-top-30 offset-sm-top-35">
                        @can ( 'registered-user', Auth::user()->role_id )
                        @can ( 'incomplete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form-next') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Resume Membership Form
                        </a>
                        @endcan
                        @can ( 'complete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @can ( 'exco-user', (Auth::user()->role_id) )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @endcan
                        @can ( 'become-members', Auth::user()->role_id )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Become a member
                        </a>
                        @endcan
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/logout" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-sign-out" aria-hidden="true"></i>Logout
                        </a>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div data-slide-bg="{!! load_asset('images/finance.jpeg') !!}" class="swiper-slide background-image item">
              <div class="swiper-slide-caption overlay">
                <div class="shell">
                  <div class="range range-sm-center">
                    <div class="cell-sm-11 cell-md-10 cell-lg-9">
                      <div data-caption-animate="fadeInUp" data-caption-delay="30s" class="shilder-header-with-divider veil reveal-md-block">We organise and formalise the informal for mutual social and economic benefits</div>
                      <h2 data-caption-animate="fadeInUp" data-caption-delay="100s" class="slider-header">Welcome to Ecoberty<br>Multipurpose Cooperative Society</h2>
                      @if (! Auth::check())
                      <div class="offset-top-30 offset-sm-top-35">
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/register" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-user-plus" aria-hidden="true"></i>Register
                        </a>
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/login" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>Login
                        </a>
                      </div>
                      @else
                      <div class="offset-top-30 offset-sm-top-35">
                        @can ( 'registered-user', Auth::user()->role_id )
                        @can ( 'incomplete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form-next') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Resume Membership Form
                        </a>
                        @endcan
                        @can ( 'complete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @can ( 'exco-user', (Auth::user()->role_id) )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @endcan
                        @can ( 'become-members', Auth::user()->role_id )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Become a member
                        </a>
                        @endcan
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/logout" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-sign-out" aria-hidden="true"></i>Logout
                        </a>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div data-slide-bg="{!! load_asset('images/slide-4.jpeg') !!}" class="swiper-slide item">
              <div class="swiper-slide-caption">
                <div class="shell">
                  <div class="range range-sm-center">
                    <div class="cell-sm-11 cell-md-10 cell-lg-9">
                      <div data-caption-animate="fadeInUp" data-caption-delay="50s" class="shilder-header-with-divider veil reveal-md-block">We believe that financial liberty cannot arise without commitment to long-term focused saving culture</div>
                      <h2 data-caption-animate="fadeInUp" data-caption-delay="100s" class="slider-header">Welcome to Ecoberty<br>Multipurpose Cooperative Society</h2>
                      @if (! Auth::check())
                      <div class="offset-top-30 offset-sm-top-35">
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/register" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-user-plus" aria-hidden="true"></i>Register
                        </a>
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/login" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="fa fa-sign-in" aria-hidden="true"></i>Login
                        </a>
                      </div>
                      @else
                      <div class="offset-top-30 offset-sm-top-35">
                        @can ( 'registered-user', Auth::user()->role_id )
                        @can ( 'incomplete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form-next') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Resume Membership Form
                        </a>
                        @endcan
                        @can ( 'complete-members', isset(Auth::user()->member) ? Auth::user()->member->completed: "" )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @can ( 'exco-user', (Auth::user()->role_id) )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/dashboard" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-plus" aria-hidden="true"></i>Dashboard
                        </a>
                        @endcan
                        @endcan
                        @can ( 'become-members', Auth::user()->role_id )
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="{{ route('members-form') }}" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-user-medrt" aria-hidden="true"></i>Become a member
                        </a>
                        @endcan
                        <a data-caption-animate="fadeInUp" data-caption-delay="250" href="/logout" style="margin-top: 0px;" class="btn btn-rect btn-icon btn-icon-right btn-primary big">
                            <i class="icon icon-xs fa fa-sign-out" aria-hidden="true"></i>Logout
                        </a>
                      </div>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-nav-wrap">
            <div class="swiper-button-prev fa fa-arrow-right"></div>
            <div class="swiper-button-next fa fa-arrow-right"></div>
          </div>
        </div>
      </section>
      <section>
        <div class="features_sec12 two">
        <div class="container"> 
        <div class="row">
          <div class="col-md-4">
            <h4 class="box">Ecoberty Cooporative is a society established to promote savings culture and support the mutual and economic development of our members by organising a pool of funds to serve as reliable source of credit and investment opportunities, which members can conveniently and affordably access for personal and business development</h4>
          </div>
          <div class="col-md-8">
            <div class="col-md-4"> 
              <div class="box"><img src="{!! load_asset('images/vision.jpeg') !!}" alt="" /> 
                <h5>Our Vision</h5> 
                <p>Our vision is to be the most supportive and responsive cooperative society in Africa.</p> 
                <br /> <br /><a href="about">Read More</a>
              </div> 
            </div>
            <div class="col-md-4">
              <div class="one_third"> 
              <div class="box"><img src="{!! load_asset('images/mission.jpeg') !!}" alt="" /> 
              <h5>Our Mission</h5> 
              <p>Our mission is to be the agent of economic and social liberation for our members through innovative, product and services.</p> 
              <br /> <a href="about">Read More</a></div> 
              </div> 
            </div>
            <div class="col-md-4">
              <div class="one_third last"> 
              <div class="box"><img src="{!! load_asset('images/preposition-small.jpg') !!}" width="320" alt="" /> 
              <h5>Value Proposition</h5> 
              <p>Our value propositions are hinged on collaborative effort, integrity, savings culture, education training and information, financial inclusion...</p> 
             <a href="about">Read More</a></div> 
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

      <section class="section-50 section-sm-100">
        <div class="shell text-center">
          <h3>What we do</h3>
          <div class="range range-xs-center offset-top-40">
            <div class="cell-xs-10 cell-sm-6 cell-md-4 height-fill">
                    <article class="icon-box">
                      <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg icon-4"></span></div>
                        <div class="box-header">
                          <h5><a href="services">Regular Savings Account (Thrift)</a></h5>
                        </div>
                      </div>
                      <div class="divider"></div>
                      <div class="box-body">
                        <p class="text-gray-05">Depending on your source of income, we have daily, monthly, weekly, quarterly and annual saving products and we can work with you to implement effective saving plan that will help you achieve your financial goals.</p>
                      </div>
                    </article>
            </div>
            <div class="cell-xs-10 cell-sm-6 cell-md-4 height-fill offset-top-40 offset-sm-top-0">
                    <article class="icon-box">
                      <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg icon-5"></span></div>
                        <div class="box-header">
                          <h5><a href="services">Personal and Business Loan (Credit)</a></h5>
                        </div>
                      </div>
                      <div class="divider"></div>
                      <div class="box-body">
                        <p class="text-gray-05">Only our members can be primary beneficiary of our loan/credit schemes. Members can take loan amount up to two times (or more) the value of their total savings subject to terms and conditions at a fair and fixed rate of interest. </p>
                      </div>
                    </article>
            </div>
            <div class="cell-xs-10 cell-sm-6 cell-md-4 height-fill offset-top-40 offset-md-top-0">
                    <article class="icon-box">
                      <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg icon-6"></span></div>
                        <div class="box-header">
                          <h5><a href="services">Equipment &amp; Leasing</a></h5>
                        </div>
                      </div>
                      <div class="divider"></div>
                      <div class="box-body">
                        <p class="text-gray-05">We finance the acquisition of cars, office and business equipment for our members at affordable rates.  Similar to personal and business loans, only our members can be the direct beneficiary of the asset to be financed. </p>
                      </div>
                    </article>
            </div>
          </div>

          <div class="range range-xs-center offset-top-40">
            <div class="cell-xs-10 cell-sm-6 cell-md-4 height-fill">
                    <article class="icon-box">
                      <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg icon-4"></span></div>
                        <div class="box-header">
                          <h5><a href="services">Education, Training & Information</a></h5>
                        </div>
                      </div>
                      <div class="divider"></div>
                      <div class="box-body">
                        <p class="text-gray-05">We provide targeted financial education service to enrich the minds of our members and non-members on various topics such as budgeting and financial planning, saving, credit, banking and other financial products and services...</p>
                      </div>
                    </article>
            </div>
            <div class="cell-xs-10 cell-sm-6 cell-md-4 height-fill offset-top-40 offset-sm-top-0">
                    <article class="icon-box">
                      <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg icon-5"></span></div>
                        <div class="box-header">
                          <h5><a href="services">Microinsurance</a></h5>
                        </div>
                      </div>
                      <div class="divider"></div>
                      <div class="box-body">
                        <p class="text-gray-05">n Nigeria, coverage of pension in the informal sector is nonexistent, however, the National Pension Commission is currently in the process of setting up an appropriate micro-pension structure...</p>
                      </div>
                    </article>
            </div>
            <div class="cell-xs-10 cell-sm-6 cell-md-4 height-fill offset-top-40 offset-md-top-0">
                    <article class="icon-box">
                      <div class="box-top">
                        <div class="box-icon"><span class="icon icon-primary icon-lg icon-6"></span></div>
                        <div class="box-header">
                          <h5><a href="services">Micropension</a></h5>
                        </div>
                      </div>
                      <div class="divider"></div>
                      <div class="box-body">
                        <p class="text-gray-05">At Ecoberty, we educate our members on risk management and encourage them to take insurance cover to mitigate risk...</p>
                      </div>
                    </article>
            </div>
          </div>
        </div>
        </div>
      </section>
      <section class="rd-parallax bg-image">
        <div data-speed="0.2" data-type="media" data-url="images/bg-image-6.jpg" class="rd-parallax-layer"></div>
        {{-- <div data-speed="0" data-type="html" class="rd-parallax-layer"> --}}
          <div class="shell section-66 section-sm-90 section-lg-bottom-120 context-dark">
            <div class="range">
              {{-- <div style="margin-top: -50px;" class="row"> --}}
                
              
              <div class="col-md-4 top-adjust">
                <h3 class="lmb">Contact Address</h3>
                <ul class="faddress">
                <li><i class="fa fa-map-marker fa-lg"></i>&nbsp;7 Micheal Otubamowo Street <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ketu Alapere,<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lagos, Nigeria</li>
                <li><i class="fa fa-phone"></i>&nbsp; +234 706 235 4492</li>
                <li><i class="fa fa-phone"></i>&nbsp; +234 806 368 0464</li>
                <li><i class="fa fa-phone"></i>&nbsp; +234 706 511 6701</li>
                <li><a href="mailto:membership@ecobertycoop.com"><i class="fa fa-envelope"></i>&nbsp;  membership@ecobertycoop.com</a></li>

                </ul>
              </div>

              <div class="col-md-4 top-adjust">
                <h3 class="lmb">Useful Links</h3>
                <div class="qlinks">
                <ul>
                <li class="current active"><a href="/" > <i ></i> Home</a>
                </li><li><a href="/about" > <i ></i> About Us</a>
                </li><li><a href="/service" > <i ></i> Services</a>
                </li><li><a href="/contact" > <i ></i> Contact Us</a>
                </li>
              </ul>
              </div>
              </div>

              <div class="col-md-4 top-adjust">
                <a class="twitter-timeline"
                    href="https://twitter.com/ecoberty"
                    data-tweet-limit="1">
              Tweets by @ecoberty</a>
              <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
          </div>
      </section>

      <footer class="page-foot section section-35 bg-ebony-clay context-dark">
          <div class="shell text-center">
            <div class="range range-sm-reverse range-sm-justify range-sm-middle">
              <div class="cell-sm-6 text-sm-right">
                <div class="group-sm group-middle">
                  <p style="color: #fff;" class="text-italic text-white">Follow Us:</p>
                  <ul class="list-inline list-inline-reset">
                    <li><a href="https://www.facebook.com/ecobertycooperativesociety" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-facebook"></a></li>
                    <li><a href="https://twitter.com/ecoberty" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-round icon-pciked-bluewood icon-xxs-smallest fa fa-instagram"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-sm-6 offset-top-15 offset-sm-top-0 text-sm-left">
                <p style="color: #fff;" class="rights text-white"><span id="copyright-year"></span><span>&nbsp;&#169;&nbsp;</span><span>Ecoberty.&nbsp;</span><a href="/terms-condition" class="link-white-v2">Terms and Condtions</a>
                </p>
              </div>
            </div>
          </div>
        </footer>
    <script src="//code.tidio.co/2wn6whfo37oksjvdmam4wnndwjsxsrv8.js"></script>
    <script src="{!! asset('js/core.min.js') !!}"></script>
    <script src="{!! asset('js/script.js') !!}"></script>
  </body>
</html>