<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index',
    'as'   => 'index',
]);

Route::get('register', 'AuthController@registerForm')->name('register');
Route::post('register', 'AuthController@create')->name('postregister');

Route::get('login', 'AuthController@loginForm')->name('login');
Route::post('login', 'AuthController@login')->name('postlogin');

Route::get('/password/reset/{token?}', 'Auth\PasswordController@getReset')->name('password.reset');
Route::post('/password/reset', 'Auth\PasswordController@postReset')->name('password.request');
Route::post('/password/sendemail', 'Auth\PasswordController@postEmail')->name('postemail');
Route::get('/password/getemail', 'Auth\PasswordController@getEmail')->name('getemail');


Route::get('logout', [
    'uses' => 'AuthController@logOut',
    'as'   => 'logout'
]);

Route::get('/terms-condition', 'PagesController@termsAndConditions')->name('TandC');
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');
Route::get('/services', 'PagesController@services');
// Route::post('/rd-mailform', 'PagesController@mailForm');
// Route::get('/membershipform', 'MembersController@membershipForm')->name('membership-form');
Route::get('/membershipform', 'MembersController@membersForm')->name('members-form');
Route::post('/membershipform', 'MembersController@postMembershipForm')->name('post-membership-form');

Route::get('/membershipform-next', 'MembersController@membersFormNext')->name('members-form-next')->middleware('auth');

Route::get('edit', [
    'uses'       => 'ProfileController@getProfileSettings',
    'as'         => 'edit-profile',
    'middleware' => ['auth'],
]);

Route::post('edit', [
    'uses' => 'ProfileController@updateProfileSettings',
    'as'   => 'post-profile',
]);

Route::get('changepassword', [
    'uses'       => 'ProfileController@getChangePassword',
    'as'         => 'changepassword',
    'middleware' => 'auth',
]);

Route::post('changepassword', [
    'uses' => 'ProfileController@postChangePassword',
    'as'   => 'post-changepassword',
]);


/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/
Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware(['auth', 'member']);
Route::get('/{purpose}/payment-form', 'DashboardController@paymentForm')->name('payment-form')->middleware(['auth', 'member']);
Route::get('/members', 'ExecutivesController@members')->name('members')->middleware(['auth', 'exco']);
Route::get('/unapproved-members', 'ExecutivesController@unapprovedMembers')->name('unapproved-members')->middleware(['auth', 'exco']);
Route::get('/users', 'ExecutivesController@users')->name('users')->middleware(['auth', 'exco']);
Route::get('/executives', 'ExecutivesController@excos')->name('excos')->middleware(['auth', 'exco']);

Route::get('/edit-user/{id}', 'ExecutivesController@editUser')->name('edit-user')->middleware(['auth', 'member']);
Route::post('/update-user/{id}', 'ExecutivesController@updateUser')->name('update-user')->middleware(['auth', 'exco']);

Route::get('/edit-exco/{id}', 'ExecutivesController@editExco')->name('edit-exco')->middleware(['auth', 'exco']);
Route::post('/update-exco/{id}', 'ExecutivesController@updateUser')->name('update-user')->middleware(['auth', 'exco']);

Route::get('/view-member/{id}', 'ExecutivesController@viewMember')->name('view-member')->middleware(['auth', 'exco']);
Route::get('/download/pdf/{id}', 'ExecutivesController@exportPdf')->middleware(['auth', 'exco']);

Route::get('/delete-member/{id}', 'ExecutivesController@deleteMember')->name('delete-member')->middleware(['auth', 'exco']);
Route::get('/restore-member/{id}', 'ExecutivesController@restoreMember')->name('restore-member')->middleware(['auth', 'exco']);
Route::get('/edit-member/{id}', 'ExecutivesController@editMember')->name('edit-member')->middleware('auth');
Route::get('/approve-member/{id}', 'ExecutivesController@approveOrDissaproveMember')->middleware(['auth', 'exco']);
Route::post('/update-member/{id}', 'ExecutivesController@updateMember')->name('update-member')->middleware(['auth', 'exco']);

Route::get('payment/callback', [
    'uses' => 'PaymentController@handleGatewayCallback',
    'middleware' => ['auth', 'member'],
]);

Route::post('/pay', [
    'uses' => 'PaymentController@redirectToGateway',
    'as' => 'pay',
]);

Route::get('/{purpose}/manual-payment', 'PaymentController@manualPayment')->middleware(['auth', 'exco']);
Route::post('/manual-payment', 'PaymentController@postManualPayment')->name('manual-record')->middleware(['auth', 'exco']);
//Loan routes
Route::get('/loan', 'DashboardController@loan')->name('loan-apply')->middleware(['auth', 'member']);
Route::get('/loan/form', 'DashboardController@loanForm')->name('loan-apply-form')->middleware(['auth', 'member', 'loan.apply']);
Route::post('/loan/form', 'DashboardController@loanApply')->name('apply-loan')->middleware(['auth', 'member', 'loan.apply']);
Route::get('/loan-id/{loanId}/guarantor-status/{id}/', 'DashboardController@guarantorStatus')->name('guarantor-status')->middleware('auth');

Route::get('/loan-applications', 'ExecutivesController@loanApplications')->name('loan-applications')->middleware(['auth', 'exco']);
Route::get('/loan-application-details/{id}', 'ExecutivesController@loanApplicationDetail')->name('view-loan')->middleware(['auth', 'exco']);

Route::get('/accept-loan/{id}', 'ExecutivesController@acceptLoan')->name('accept-loan')->middleware(['auth', 'exco']);
Route::get('/reject-loan/{id}', 'ExecutivesController@rejectLoan')->name('reject-loan')->middleware(['auth', 'exco']);
